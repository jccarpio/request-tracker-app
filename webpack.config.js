const HtmlWebpackPlugin = require('html-webpack-plugin')
const CleanWebpackPlugin = require('clean-webpack-plugin')
const UglifyJsPlugin = require('uglifyjs-webpack-plugin')
const webpack = require('webpack')

const config = {
  entry: `${__dirname}/src/index.js`,
  output: {
      path: `${__dirname}/build/`,
      filename: 'app-[hash].js',
      publicPath: '/',
  },
  resolve: {
      modules: ['node_modules', 'src', './'],
      extensions: ['.ts', '.js'],
  },
  module: {
      loaders: [
          {
              test: /\.js$/,
              exclude: /(node_modules|bower_components)/,
              loaders: ['babel-loader'],
          },
          {
              test: /\.css$/,
              use: ['style-loader', 'css-loader'],
          },
          {
              test: /\.woff2?(.*)$/,
              loader: 'url-loader?limit=10000&mimetype=application/font-woff',
          },
          {
              test: /\.ttf(.*)$/,
              loader: 'file-loader',
          },
          {
              test: /\.eot(.*)$/,
              loader: 'file-loader',
          },
          {
              test: /\.svg(.*)$/,
              loader: 'file-loader',
          },
          {
              test: /\.(json|geojson)$/,
              loader: 'json-loader',
          },
          {
              test: /\.(jpe?g|png|gif)$/i,
              loader: 'url-loader',
          },
      ],
  },
  devtool: '#inline-source-map',
  devServer: {
      contentBase: `${__dirname}/`,
      port: process.env.PORT || '3000',
      proxy: {
          '/api': {
              target: 'https://testrt.peplink.com/',
              changeOrigin:true
          },
      },
  },
  plugins: [
      new CleanWebpackPlugin([`${__dirname}/build`]),
      new HtmlWebpackPlugin({ template: `${__dirname}/index.html` }),
  ],
}

config.plugins.push(
  new webpack.DefinePlugin({
    ENVIRONMENT: JSON.stringify('development'),
  })
)

module.exports = config
