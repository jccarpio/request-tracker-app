const HtmlWebpackPlugin = require('html-webpack-plugin')
const CleanWebpackPlugin = require('clean-webpack-plugin')
const UglifyJsPlugin = require('uglifyjs-webpack-plugin')
const webpack = require('webpack')
const config = require('./webpack.config')

config.plugins = [
  ...config.plugins,
  new webpack.DefinePlugin({
    ENVIRONMENT: JSON.stringify('production'),
  }),
  new UglifyJsPlugin()
]

module.exports = config
