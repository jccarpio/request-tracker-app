## Peplink Ecommerce Request Tracker App

#### Building commands
There are 2 ways to make a build, on the root of the directory run:

1. `yarn build` for staging environment
2. `yarn build:prod` for production or minified version


#### Running Locally
1. `yarn start`
2. Go to http://localhost:3000

#### Technologies Used
- `react`
- `react-router`
- `styled-components`
- `mobx`
