export const TICKET_STATUSES = [
  { text: 'New', value: 'NEW' },
  { text: 'Awaiting owner feedback', value: 'AWAITING_OWNER' },
  { text: 'Awaiting customer feedback', value: 'AWAITING_CUSTOMER' },
  { text: 'Solution provided', value: 'PROVIDE_SOLUTION' },
  { text: 'Under RMA procedure', value: 'RMA_PROCESS' },
  { text: 'Resolved', value: 'RESOLVED' },
  // { text: 'Reopen', value: 'REOPEN' },
  { text: 'Closed', value: 'CLOSED' },
]

export const TICKET_PRIORITY = [
  { text: 'Low', value: 'LOW' },
  { text: 'Medium', value: 'MEDIUM' },
  { text: 'High', value: 'HIGH' },
  { text: 'Urgent', value: 'URGENT' },
]
