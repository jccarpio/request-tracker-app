/* @flow */

import MainLayout from '../Styled/MainLayout'
import React, { Component } from 'react'

export default class Tools extends Component {
  render() {
    return (
      <MainLayout>
        <section className="main-content">this is the tools page</section>
      </MainLayout>
    )
  }
}
