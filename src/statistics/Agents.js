/* @flow */

import {
  Panel as _Panel,
  Button,
  ButtonGroup,
  ButtonToolbar,
  Col,
  ControlLabel,
  Form,
  FormControl,
  FormGroup,
  Row,
} from 'react-bootstrap'
import { action, computed, observable } from 'mobx'
import { FormAutoComplete, PaddedChartContainer } from './styled'
import { observer } from 'mobx-react'
import PanelChart from '../common/Chart/PanelChart'
import React, { Component } from 'react'
import SelectDropdown from '../common/SelectDropdown'

const panelChartData = {
  labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
  datasets: [
    {
      label: 'Awaiting Feedback',
      fill: false,
      backgroundColor: '#3DA3E8',
      borderColor: '#3DA3E8',
      lineTension: 0,
      data: [5, 15, 30, 45, 25, 15, 30],
    },
  ],
}

@observer
export default class AgentsChartPage extends Component {
  @observable
  selectedAgent = {
    value: '',
    text: '',
  }

  @action.bound
  handleAgentChange(selectedOption) {
    if (selectedOption) {
      this.selectedAgent = selectedOption
    } else {
      this.selectedAgent = {
        value: '',
        text: '',
      }
    }
  }
  render() {
    const { text, value } = this.selectedAgent
    return (
      <div>
        <Row>
          <PaddedChartContainer lg={12}>
            <h4>Agents</h4>
            <ButtonToolbar className="pull-right">
              <ButtonGroup bsSize="xsmall">
                <Button bsStyle="warning">Week</Button>
                <Button>Month</Button>
                <Button>Quarter</Button>
                <Button>Year</Button>
              </ButtonGroup>
            </ButtonToolbar>
            <Row>
              <Col lg={4}>
                <FormAutoComplete inline>
                  <SelectDropdown
                    options={[
                      {
                        text: 'Agent 1',
                        value: 1,
                      },
                      {
                        text: 'Agent 2',
                        value: 1,
                      },
                    ]}
                    onChange={this.handleAgentChange}
                    placeholder="Enter agent name"
                    value={value}
                  />
                  <Button type="submit" bsStyle="warning" bsSize="sm">
                    Show
                  </Button>
                </FormAutoComplete>
                <h4>{text}</h4>
              </Col>
            </Row>
          </PaddedChartContainer>
        </Row>
        <Row>
          <PaddedChartContainer lg={12}>
            <Col lg={6}>
              <PanelChart title="Ticket assigned" data={panelChartData} />
            </Col>
            <Col lg={6}>
              <PanelChart title="Ticket resolved" data={panelChartData} />
            </Col>
          </PaddedChartContainer>
        </Row>
        <Row>
          <PaddedChartContainer lg={12}>
            <Col lg={6}>
              <PanelChart title="1st response SLA %" data={panelChartData} />
            </Col>
            <Col lg={6}>
              <PanelChart title="Resolution SLA" data={panelChartData} />
            </Col>
          </PaddedChartContainer>
        </Row>
        <Row>
          <PaddedChartContainer lg={12}>
            <Col lg={6}>
              <PanelChart title="FCR %" data={panelChartData} />
            </Col>
            <Col lg={6}>
              <PanelChart title="Ticket escalated" data={panelChartData} />
            </Col>
          </PaddedChartContainer>
        </Row>
        <Row>
          <PaddedChartContainer lg={12}>
            <Col lg={6}>
              <PanelChart title="Average 1st response time" data={panelChartData} />
            </Col>
            <Col lg={6}>
              <PanelChart title="Average resolution time" data={panelChartData} />
            </Col>
          </PaddedChartContainer>
        </Row>
      </div>
    )
  }
}
