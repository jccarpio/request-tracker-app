import { Col, Form } from 'react-bootstrap'
import styled from 'styled-components'

const PaddedChartContainer = styled(Col)`
  padding: 10px 50px;
`
const FormAutoComplete = styled(Form)`
  .Select,
  button {
    display: inline-flex;
  }
  .Select {
    width: 80%;
  }
  button {
    margin: 0 5px;
  }
`

export { PaddedChartContainer, FormAutoComplete }
