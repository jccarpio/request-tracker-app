/* @flow */

import { Col, Grid, Row } from 'react-bootstrap'
import { Match, Redirect, Route, Router, Switch } from 'react-router'
import { NavLink } from 'react-router-dom'
import { observer } from 'mobx-react'
import Agents from './Agents'
import Avatar from '../common/Avatar'
import Groups from './Groups'
import MainLayout from '../Styled/MainLayout'
import React, { Component } from 'react'
import styled from 'styled-components'
import Tickets from './Tickets'

const PageHeader = styled(Col)`
  margin-top: 20px;
  .navbar-inner {
    list-style-type: none;
    .nav-bar-item {
      text-decoration: none;
      display: inline-block;
      margin-right: 50px;
      a {
        div {
          width: 70px;
          height: 70px;
          background-position: center center;
          background-size: cover;
          &.ticket {
            background-image: url('${require('../images/ticket-default.png')}');
          }
          &.agent {
            background-image: url('${require('../images/agent-default.png')}');
          }
          &.groups {
            background-image: url('${require('../images/groups-default.png')}');
          }
        }
        &.active {
          div {
            &.ticket {
              background-image: url('${require('../images/ticket-active.png')}');
            }
            &.agent {
              background-image: url('${require('../images/agent-active.png')}');
            }
            &.groups {
              background-image: url('${require('../images/groups-active.png')}');
            }
          }
        }
      }
    }
  }
`

export default class StatisticsPage extends Component {
  render() {
    const { match } = this.props
    console.log('this is the match', match)
    return (
      <MainLayout>
        <Grid className="main-content">
          <Row>
            <PageHeader lg={12}>
              <h4>Statistics</h4>
              <ul className="navbar-inner">
                <li className="nav-bar-item">
                  <NavLink to={`${match.url}/tickets`} activeClassName="active">
                    <div className="ticket" />
                  </NavLink>
                </li>
                <li className="nav-bar-item">
                  <NavLink to={`${match.url}/agents`} activeClassName="active">
                    <div className="agent" />
                  </NavLink>
                </li>
                <li className="nav-bar-item">
                  <NavLink to={`${match.url}/groups`} activeClassName="active">
                    <div className="groups" />
                  </NavLink>
                </li>
              </ul>
            </PageHeader>
          </Row>
          <Row>
            <Col lg={12}>
              <Route exact path={`${match.path}`} render={() => <Redirect to={`${match.path}/tickets`} />} />
              <Route exact path={`${match.path}/tickets`} component={Tickets} />
              <Route exact path={`${match.path}/agents`} component={Agents} />
              <Route exact path={`${match.path}/groups`} component={Groups} />
            </Col>
          </Row>
        </Grid>
      </MainLayout>
    )
  }
}
