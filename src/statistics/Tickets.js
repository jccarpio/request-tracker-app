/* @flow */

import { Panel as _Panel, Button, ButtonGroup, ButtonToolbar, Col, Grid, Row } from 'react-bootstrap'
import { NavLink } from 'react-router-dom'
import { observer } from 'mobx-react'
import { PaddedChartContainer } from './styled'
import Avatar from '../common/Avatar'
import LineChart from '../common/Chart/LineChart'
import MainLayout from '../Styled/MainLayout'
import React, { Component } from 'react'

const data = {
  labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
  datasets: [
    {
      label: 'New',
      fill: false,
      backgroundColor: '#FD6585',
      borderColor: '#FD6585',
      lineTension: 0,
      data: [10, 20, 30, 40, 35, 25, 15],
    },
    {
      label: 'Awaiting Feedback',
      fill: false,
      backgroundColor: '#3DA3E8',
      borderColor: '#3DA3E8',
      lineTension: 0,
      data: [5, 15, 30, 45, 25, 15, 30],
    },
    {
      label: 'Solution Provided',
      fill: false,
      backgroundColor: '#FECC60',
      borderColor: '#FECC60',
      lineTension: 0,
      data: [15, 30, 45, 60, 30, 20, 10],
    },
    {
      label: 'Under RMA',
      backgroundColor: '#51C0BF',
      borderColor: '#51C0BF',
      fill: false,
      lineTension: 0,
      data: [40, 30, 20, 35, 70, 25, 15],
    },
  ],
}

export default class TicketChartPage extends Component {
  render() {
    return (
      <div>
        <Row>
          <Col lg={12}>
            <h4>Tickets</h4>
            <ButtonToolbar className="pull-right">
              <ButtonGroup bsSize="xsmall">
                <Button bsStyle="warning">Week</Button>
                <Button>Month</Button>
                <Button>Quarter</Button>
                <Button>Year</Button>
              </ButtonGroup>
            </ButtonToolbar>
          </Col>
        </Row>
        <Row>
          <PaddedChartContainer lg={12}>
            <LineChart data={data} />
          </PaddedChartContainer>
        </Row>
      </div>
    )
  }
}
