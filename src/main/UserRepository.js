/* @flow */

import { action, computed, observable } from 'mobx'
import _ from 'lodash'
export default class UserRepository {
  @observable
  user = {
    email: 'kenosp@peplink.com',
    role: 'ROLE_ADMIN',
    fullName: 'Keno San Pedro',
    id: 1,
  }

  @observable currentUserId: number = 1

  @observable
  users = [
    {
      email: 'kenosp@peplink.com',
      role: 'ROLE_ADMIN',
      fullName: 'Keno San Pedro',
      id: 1,
    },
    {
      email: 'jcarpio@peplink.com',
      role: 'ROLE_AGENT',
      fullName: 'John Carpio',
      id: 2,
    },
    {
      email: 'ericahmad@peplink.com',
      role: 'ROLE_USER',
      fullName: 'Eric Ahmad',
      id: 3,
    },
  ]

  @action.bound
  setCurrentUser(currentUserId: number) {
    this.currentUserId = currentUserId ? parseInt(currentUserId) : 1
  }

  @computed
  get currentUser(): any {
    const currentUser = _.find(this.users, item => {
      return item.id === this.currentUserId
    })

    return currentUser
  }
}
