/* @flow */

import { COLOR_INVERSE } from '../Styled/themeColours'
import { inject, observer } from 'mobx-react'
import Link from '../common/Link'
import logo from '../images/logo.svg'
import Navigation from './Navigation'
import React, { Component } from 'react'
import SelectDropdown from '../common/SelectDropdown'
import styled from 'styled-components'
export const HEADER_HEIGHT = 100

const StyledHeader = styled.header`
  height: ${HEADER_HEIGHT}px;
  position: fixed;
  top:0;
  left: 0;
  width: 100%;
  background:#FFF;
  overflow: hidden;
  z-index: 10;
  .logo-container {
    padding: 10px 0;
  }

  .brand-logo {
    display: inline-block;
    img {
      height: 35px;
      display: inline-block;
      vertical-align: : middle;
      margin-right: : 10px;
    }
    .brand-text {
      color: ${COLOR_INVERSE};
      font-size:20px;
      margin-left:10px;
      display: inline-block;
      vertical-align: : middle;
    }
  }

  .pull-right {
    padding:8px 10px;
    button {
      width:98px;
      border:none;
    }
  }
  .options-container{
    display: inline-flex;
    .drop-down-container{
      margin-left: 10px;
    }
  }
`

type Props = {
  history: Object,
}

@inject('userRepository')
@observer
export default class Header extends Component<Props> {
  render() {
    const isNewTicketRoute = this.props.history.location.pathname === '/ticket/new'
    const { setCurrentUser, users, currentUser } = this.props.userRepository
    const options = users.map(item => ({
      text: item.role,
      value: item.id,
    }))

    return (
      <StyledHeader>
        <div className="width-container">
          <div className="logo-container">
            <a href="/" className="brand-logo">
              <img src={logo} />
              <h4 className="brand-text">Support Ticketing System</h4>
            </a>
            <div className="pull-right options-container">
              {!isNewTicketRoute && (
                <Link to="/ticket/new" bsStyle="secondary">
                  New Ticket
                </Link>
              )}
              <div className="drop-down-container">
                <SelectDropdown
                  value={currentUser.id}
                  options={options}
                  name={currentUser.name}
                  onChange={event => setCurrentUser(event.target.value)}
                  withOutDefault
                />
              </div>
            </div>
          </div>
        </div>
        <Navigation history={this.props.history} />
      </StyledHeader>
    )
  }
}
