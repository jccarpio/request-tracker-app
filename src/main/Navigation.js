/* @flow */

import { COLOR_SECONDARY, COLOR_TERTIARY } from '../Styled/themeColours'
import { NavLink } from 'react-router-dom'
import React, { Component } from 'react'
import styled from 'styled-components'

const StyledNavigation = styled.nav`
  background-color: ${COLOR_SECONDARY};
  .navbar-inner {
    padding: 0;
    width: 100%;
    list-style-type: none;
    margin: 0;
  }
  .nav-bar-item {
    padding: 0;
    display: inline-block;
    color: rgba(255, 255, 255, 1);
    a {
      display: block;
      padding: 10px;
      color: rgba(255, 255, 255, 1);
      &.active {
        background-color: ${COLOR_TERTIARY};
      }
    }
  }
`

type Props = {
  history: any,
}

export default class Navigation extends Component {
  props: Props
  render() {
    return (
      <StyledNavigation>
        <div className="width-container">
          <ul className="navbar-inner">
            <li className="nav-bar-item">
              <NavLink to="/dashboard" activeClassName="active">
                Dashboard
              </NavLink>
            </li>
            <li className="nav-bar-item">
              <NavLink to="/ticket-conversation/8030003" activeClassName="active">
                Ticket Conversation
              </NavLink>
            </li>
            <li className="nav-bar-item">
              <NavLink to="/ticket/list" activeClassName="active">
                Ticket List
              </NavLink>
            </li>
            <li className="nav-bar-item">
              <NavLink to="/tools" activeClassName="active">
                Tools
              </NavLink>
            </li>
            <li className="nav-bar-item">
              <NavLink to="/statistics" activeClassName="active">
                Statistics
              </NavLink>
            </li>
          </ul>
        </div>
      </StyledNavigation>
    )
  }
}
