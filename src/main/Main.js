/* @flow */

import 'react-confirm-alert/src/react-confirm-alert.css'
import { createHashHistory } from 'history'
import { HEADER_HEIGHT } from '../main/Header'
import { Redirect, Route, Router } from 'react-router'
import Dashboard from '../dashboard/'
import Header from '../main/Header'
import NewTicketPage from '../ticket/add'
import Notifications from 'react-notify-toast'
import React, { Component } from 'react'
import Statistics from '../statistics/'
import styled from 'styled-components'
import TicketConversationPage from '../ticket/TicketConversationPage'
import TicketListPage from '../ticket/list/'
import Tools from '../tools/Tools'

const StyledMainContainer = styled.section`
  padding-top: ${HEADER_HEIGHT}px;
  height: 100%;
  box-sizing: border-box;
  overflow-x: hidden;
`
const TOASTER_DEFAULTS = {
  timeout: 2000,
  colors: {
    error: {
      color: '#CC3333',
      backgroundColor: '#FFDDDD',
    },
    success: {
      color: '#009900',
      backgroundColor: '#CEFFCE',
    },
    warning: {
      color: '#FF6600',
      backgroundColor: '#FFFFCC',
    },
    info: {
      color: '#FF6600',
      backgroundColor: '#FFFFCC',
    },
  },
}

const history = createHashHistory()
class RouterScreen extends Component {
  render() {
    return (
      <Router history={history}>
        <StyledMainContainer>
          <Header history={history} />
          <Notifications options={TOASTER_DEFAULTS} />
          <Route exact path="/" render={() => <Redirect to={'/dashboard'} />} />
          <Route exact path="/dashboard" component={Dashboard} />
          <Route exact path="/ticket-conversation" component={TicketConversationPage} />
          <Route exact path="/ticket-conversation/:id" component={TicketConversationPage} />
          <Route exact path="/ticket/new" component={NewTicketPage} />
          <Route exact path="/ticket/list" component={TicketListPage} />
          <Route path="/tools" component={Tools} />
          <Route path="/statistics" component={Statistics} />
        </StyledMainContainer>
      </Router>
    )
  }
}

export default RouterScreen
