/* @flow */

import { Grid as _Grid, Button, Col, ControlLabel, Form, FormControl, FormGroup, HelpBlock, Row } from 'react-bootstrap'
import { action, computed, observable } from 'mobx'
import { type ApiError } from '../../Helpers/API'
import { autobind } from 'core-decorators'
import { createTicket } from '../services'
import { email, required } from '../../Helpers/Validations'
import { getInquiryTypes } from '../services/'
import { MainLayout } from '../../Styled'
import { observer } from 'mobx-react'
import _ from 'lodash'
import Countries from 'country-list'
import CreateTicketForumModal from './CreateTicketForumModal'
import ErrorsList from '../../common/ErrorsList'
import Icon from '../../Styled/Icon'
import React, { Component } from 'react'
import SearchableSelectDropdown from '../../common/SearchableSelectDropdown'
import styled from 'styled-components'
import WysiwygEditor from '../../common/WysiwygEditor'
const countryList = Countries()
  .getNames()
  .map(country => ({ value: country, label: country }))

const Grid = styled(_Grid)`
  padding-bottom: 10px;
  .errors-list {
    margin-bottom: 20px;
  }
  .control-label {
    text-align: left !important;
  }
  .wysiwyg-wrapper {
    background: #fff;
    padding: 0;
  }
  .has-error {
    .wysiwyg-editor {
      border: 1px solid #a94442;
    }
  }
`
const StyledForm = styled(Form)`
  .help-block {
    font-style: italic;
    color: red;
    font-size: 12;
  }
`

const AddDeviceAction = styled.a`
  margin: 10px 0;
  color: inherit;
  display: block;
  cursor: pointer;
  &:hover {
    color: inherit;
  }
`
const PaddedCol = styled(Col)`
  padding: 5px 15px;
`
const PaddedColAbsolute = styled(Col)`
  position: absolute;
  right: -15px;
  margin-top: 8px;
  a {
    display: block;
    padding: 6px;
    i.fa {
      &.text-danger {
        font-size: 18px;
      }
    }
  }
`

const RelativeRow = styled(Row)`
  position: relatve;
`
const TitleContainer = styled.div`
  h3,
  div {
    display: inline-block;
  }
  div {
    height: 26px;
    vertical-align: middle;
    padding: 0 15px;
  }
`

type FormField = {
  name: string,
  label: string,
  required?: boolean,
  type?: string,
}

@observer
export default class NewTicketPage extends Component {
  static FIELDS: Object = {
    name: { label: 'Name', required: true },
    email: { label: 'Email', type: 'email', required: true },
    subject: { label: 'Ticket Subject', required: true },
    inquiryTypeId: { label: 'Type of Inquiry', required: true, type: 'select' },
    inquiryDetail: { label: 'Inquiry Details', type: 'wysiwyg', required: true },
    country: { label: 'Country', type: 'select' },
    pointOfPurchase: { label: 'Point of Purchase' },
    deviceDetails: {
      label: 'Device Details',
      fields: [
        {
          serialNumber: { label: 'S/N' },
          firmwareVersion: { label: 'Firmware Version' },
        },
      ],
    },
    attachmentUniqueId: { type: 'addon', required: false },
  }

  @observable
  formValues = {
    attachmentUniqueId: { value: '', hasChanged: false, hasVirus: false },
    name: { value: '', hasChanged: false },
    email: { value: '', hasChanged: false },
    subject: { value: '', hasChanged: false },
    inquiryTypeId: { value: '', hasChanged: false },
    inquiryDetail: { value: '', hasChanged: false, hasText: false },
    country: { value: '', hasChanged: false },
    deviceDetails: {
      values: [
        {
          serialNumber: '',
          firmwareVersion: '',
        },
      ],
      hasChanged: false,
    },
    pointOfPurchase: { value: '', hasChanged: false },
  }

  @observable apiErrors: ?Array<ApiError> = null
  @observable isCreatingTicket: boolean = false
  @observable isUploadingAttachments: boolean = false
  @observable isCreateTicketForumModalOpen = false
  @observable inquiryTypes: ?Array<InquiryTypes> = []

  @computed
  get formErrors(): any {
    const { validateField } = this
    return {
      name: validateField('name'),
      email: validateField('email'),
      subject: validateField('subject'),
      inquiryTypeId: validateField('inquiryTypeId'),
      inquiryDetail: validateField('inquiryDetail'),
      // attachmentFiles: validateField('attachmentFiles'),
    }
  }

  @action.bound
  toggleMergeModal() {
    this.isCreateTicketForumModalOpen = !this.isCreateTicketForumModalOpen
  }

  @autobind
  validateField(fieldName: string): ?string {
    const { required: isRequired, type } = NewTicketPage.FIELDS[fieldName]
    const { value, hasText } = this.formValues[fieldName]
    let error = null

    if (isRequired && type !== 'wysiwyg') {
      error = required(value)
    } else if (isRequired && type === 'wysiwyg') {
      error = required(hasText)
    }
    // if (!error && fieldName === 'attachmentFiles') {
    //   const hasVirus = _.filter(value, { isClean: false }).length
    //   return hasVirus ? 'Virus found, one or more of your attachment(s) was infected, please remove it.' : null
    // }
    if (!error && type === 'email') {
      return email(value)
    }

    return error
  }

  @computed
  get isFormDisabled(): boolean {
    const errors = _.values(this.formErrors)
    return (
      this.isCreatingTicket ||
      Boolean(_.find(errors, error => Boolean(error))) ||
      this.isUploadingAttachments ||
      this.formValues.attachmentUniqueId.hasVirus
    )
  }

  @action
  setApiErrors(apiErrors: Array<ApiError>) {
    this.apiErrors = apiErrors
  }

  @action
  clearApiErrors() {
    this.apiErrors = null
  }

  @action.bound
  handleInputChange(e: *) {
    this.formValues[e.target.name].value = e.target.value
  }

  @action.bound
  handleCountryChange(selectedOption) {
    this.formValues.country.value = selectedOption.value
  }

  @action.bound
  handleInquiryTypeChange(selectedOption) {
    this.formValues.inquiryTypeId.value = selectedOption.value
  }

  @action.bound
  handleInputBlur(e: *) {
    this.formValues[e.target.name] = {
      value: this.formValues[e.target.name].value,
      hasChanged: true,
    }
  }

  @action.bound
  handleDeviceDetailsChange(index: number, field, value) {
    let deviceDetailsValues = [...this.formValues.deviceDetails.values]
    deviceDetailsValues[index] = {
      ...deviceDetailsValues[index],
      [field]: value,
    }
    this.formValues.deviceDetails = {
      ...this.formValues.deviceDetails,
      values: [...deviceDetailsValues],
      hasChanged: true,
    }
  }

  @action.bound
  handleEditorChange(value: string, hasText: boolean) {
    this.formValues.inquiryDetail.value = value
    this.formValues.inquiryDetail.hasText = hasText
  }

  @action.bound
  handleEditorBlur(value: string, hasText: boolean) {
    this.formValues.inquiryDetail = {
      value,
      hasText,
      hasChanged: true,
    }
  }

  @action.bound
  onAttachmentUploadStart() {
    this.isUploadingAttachments = true
  }

  @action.bound
  onAttachmentUploadFailed() {
    this.isUploadingAttachments = false
  }

  @action.bound
  handleAttachmentChange(attachmentUniqueId: string, hasVirus: boolean) {
    this.isUploadingAttachments = false
    this.formValues.attachmentUniqueId = {
      value: attachmentUniqueId,
      hasVirus: hasVirus,
    }
  }

  @action.bound
  async handleSubmit(e: *) {
    e.preventDefault()
    if (this.isFormDisabled) {
      return
    }
    this.clearApiErrors()
    this.isCreatingTicket = true
    const formValuePayload = _.mapValues(this.formValues, item => item.value || item.values)
    if (formValuePayload.deviceDetails) {
      formValuePayload.deviceDetails = _.filter(formValuePayload.deviceDetails, d => !_.isEmpty(d.serialNumber))
    }
    const { data, errors } = await createTicket(formValuePayload)
    if (!_.isEmpty(data)) {
      this.onCreateTicketSuccess(data)
    } else if (errors) {
      this.onCreateTicketFailed(errors)
    }
  }

  @action
  onCreateTicketSuccess(data: any) {
    this.isCreatingTicket = false
    window.location.reload()
  }

  @action
  onCreateTicketFailed(errors: Array<ApiError>) {
    this.setApiErrors(errors)
    this.isCreatingTicket = false
    this.refs.mainContent.scrollTop = 0
  }

  renderRequiredMarker() {
    return <span className="required">*</span>
  }

  @action.bound
  addDeviceInfoRow(e: *) {
    const deviceInfoRowmFormVals = {
      serialNumber: '',
      firmwareVersion: '',
    }

    this.formValues['deviceDetails'].values.push(deviceInfoRowmFormVals)
  }

  @action.bound
  removeDeviceInfoRow(index: number) {
    this.formValues['deviceDetails'].values.splice(index, 1)
  }

  @action.bound
  async getInquiryTypes() {
    const { data, errors } = await getInquiryTypes()

    if (!errors.length) {
      this.inquiryTypes = _.map(data, d => ({
        value: d.id,
        label: d.description,
      }))
    }
  }

  componentWillMount() {
    this.getInquiryTypes()
  }

  renderFields(): * {
    return Object.keys(NewTicketPage.FIELDS).map((name, index) => {
      const { label, type = 'text', required }: FormField = NewTicketPage.FIELDS[name]
      const { value, hasChanged } = this.formValues[name]
      const error = this.formErrors[name]
      const showError = Boolean(error) && hasChanged
      const attachmentsHasChanged = this.formValues.attachmentUniqueId.hasChanged
      return (
        <FormGroup key={index} validationState={showError ? 'error' : null}>
          <Col lg={4} componentClass={ControlLabel} className="text-left">
            {label}
            {required && this.renderRequiredMarker()}
          </Col>
          <Col lg={8}>
            {name === 'deviceDetails' && (
              <div>
                <Row>
                  {_.map(NewTicketPage.FIELDS[name].fields[0], (field, key) => (
                    <Col lg={6} key={key} componentClass={ControlLabel}>
                      {field.label}
                    </Col>
                  ))}
                </Row>
                {_.map(this.formValues[name].values, (fields, key) => {
                  return (
                    <RelativeRow>
                      {_.map(fields, (field, index) => {
                        return (
                          <PaddedCol lg={6} key={index}>
                            <FormControl
                              className={index}
                              value={value}
                              name={index}
                              type={type}
                              onChange={e => {
                                this.handleDeviceDetailsChange(key, index, e.target.value)
                              }}
                              onBlur={e => {
                                this.handleDeviceDetailsChange(key, index, e.target.value)
                              }}
                            />
                          </PaddedCol>
                        )
                      })}
                      {key > 0 && (
                        <PaddedColAbsolute>
                          <a
                            href="javascript:void(0)"
                            onClick={() => {
                              this.removeDeviceInfoRow(key)
                            }}
                          >
                            <Icon iconName="minus-circle" sClass="text-danger" large />
                          </a>
                        </PaddedColAbsolute>
                      )}
                    </RelativeRow>
                  )
                })}
                <Row>
                  <Col lg={12}>
                    <AddDeviceAction herf="javascript:void(0)" onClick={this.addDeviceInfoRow}>
                      <Icon iconName="plus-circle" sClass="text-success" /> Add another device
                    </AddDeviceAction>
                  </Col>
                </Row>
              </div>
            )}
            {type === 'wysiwyg' && (
              <WysiwygEditor
                onChange={this.handleEditorChange}
                onBlur={this.handleEditorBlur}
                onAttachmentUploadStart={this.onAttachmentUploadStart}
                onAttachmentUploadComplete={this.handleAttachmentChange}
                onAttachmentUploadFailed={this.onAttachmentUploadFailed}
                textFieldHelpBlock={showError ? error : null}
              />
            )}
            {type === 'select' && (
              <SearchableSelectDropdown
                options={name === 'country' ? countryList : this.inquiryTypes}
                onChange={name === 'country' ? this.handleCountryChange : this.handleInquiryTypeChange}
                placeholder="Select a country"
                value={value}
              />
            )}
            {['email', 'text'].indexOf(type) > -1 &&
              name !== 'deviceDetails' && (
                <div>
                  <FormControl
                    className={name}
                    value={value}
                    name={name}
                    type={type}
                    onChange={this.handleInputChange}
                    onBlur={this.handleInputBlur}
                  />
                  {showError && <HelpBlock>{error}</HelpBlock>}
                </div>
              )}
          </Col>
        </FormGroup>
      )
    })
  }

  render() {
    return (
      <MainLayout>
        <section className="main-container">
          <section className="main-content" ref="mainContent">
            <Grid>
              <Row>
                <Col lg={10}>
                  <TitleContainer>
                    <h3>Create new ticket </h3>
                    <div>
                      <a href="javascript:void(0);" onClick={this.toggleMergeModal}>
                        Create ticket from forum URL
                      </a>
                    </div>
                  </TitleContainer>
                  {this.apiErrors && <ErrorsList errors={this.apiErrors} />}
                  <StyledForm horizontal onSubmit={this.handleSubmit}>
                    <FormGroup>
                      <Col lg={4} componentClass={ControlLabel} className="text-left">
                        ({this.renderRequiredMarker()}) Required Fields
                      </Col>
                    </FormGroup>
                    {this.renderFields()}
                    <div className="text-center">
                      <Button type="submit" bsStyle="warning" disabled={this.isFormDisabled}>
                        {this.isCreatingTicket ? 'Creating...' : 'Create Ticket'}
                      </Button>{' '}
                      <Button type="button" disabled={this.isFormDisabled}>
                        Cancel
                      </Button>
                    </div>
                  </StyledForm>
                </Col>
              </Row>
            </Grid>
          </section>
        </section>
        {this.isCreateTicketForumModalOpen && (
          <CreateTicketForumModal showModal={this.isMergeModalOpen} toggleModal={this.toggleMergeModal} />
        )}
      </MainLayout>
    )
  }
}
