/* @flow */

import { Button, ControlLabel, Form, FormControl, FormGroup } from 'react-bootstrap'
import Modal from '../../common/Modal'
import React, { Component } from 'react'

type Props = {
  toggleModal: () => void,
}

export default class CreateTicketForumModal extends Component {
  props: Props
  render() {
    const { toggleModal } = this.props
    return (
      <Modal
        header={'Create new ticket from Forum Post:'}
        footer={
          <div>
            <Button bsSize="small" bsStyle="warning" onClick={toggleModal}>
              Confirm
            </Button>{' '}
            <Button bsSize="small" onClick={toggleModal}>
              Cancel
            </Button>
          </div>
        }
        showModal
        onHide={toggleModal}
      >
        <Form>
          <FormGroup>
            <ControlLabel>Paste forum post URL here:</ControlLabel>{' '}
            <FormControl type="text" placeholder="ex. http://forum.peplink.com/forum-id" />
          </FormGroup>
        </Form>
      </Modal>
    )
  }
}
