/* @flow */

import { action, observable } from 'mobx'
import { autobind } from 'core-decorators'
import { Button, Link } from '../Styled'
import { Col, Row } from 'react-bootstrap'
import { getFormattedDateString } from '../Helpers/dateFormatters'
import { inject, observer } from 'mobx-react'
import _ from 'lodash'
import BasicInfo from './BasicInfo/'
import CollapsibleWidget from '../common/CollapsibleWidget'
import ContentContainer from '../Styled/ContentContainer'
import DeviceDetails from './DeviceDetails/'
import Icon from '../common/Icon'
import React, { Component } from 'react'
import SelectDropdown from '../common/SelectDropdown'
import styled from 'styled-components'
import TagsInputForm from '../common/TagsInputForm'
import TicketAttachments from './TicketAttachments'
import TicketRepository from './TicketRepository'
import type { ApiError } from '../Helpers/API'

type Props = {
  ticketRepository: TicketRepository,
  ticketId: number,
}

const TEMP_REDIRECT = '/dashboard'

const StyledSidebarContent = styled.div`
  padding: 10px;
  .detail-row {
    padding-bottom: 5px;
    .detail-label,
    .detail-value {
      line-height: 34px;
    }
  }
`

const StyledErrorMessage = styled.div`
  padding: 10px;
  text-align: center;
  .error-message {
    margin-bottom: 20px;
  }
`

@inject('ticketRepository')
@observer
export default class DashboardSideBar extends Component {
  props: Props

  @observable isGettingTicketInfoDropdownValues: boolean = false
  @observable ticketPriorities: Array<any> = []
  @observable ticketQueues: Array<any> = []
  @observable isOpenDeviceDetailsModal: boolean = false
  @observable isOpenBasicInfoModal: boolean = false
  @observable deviceDetailsMode: string = 'viewMode'

  componentWillMount() {
    this.props.ticketRepository.getInquiryTypes()
  }

  componentDidMount() {
    this.onGetTicketDetails(this.props.ticketId)
  }

  componentWillReceiveProps(nextProps: any) {
    if (this.props.ticketId === nextProps.ticketId) {
      return
    }

    this.onGetTicketDetails(nextProps.ticketId)
  }

  @observable ticketTags: Array<string> = []

  @action.bound
  onChangeTags(tags: Array<string>) {
    const { ticketId, ticketRepository } = this.props
    this.ticketTags = tags
    ticketRepository.onChangeTags(ticketId, tags)
  }

  @action.bound
  async onGetTicketDetails(id: number) {
    const { ticketRepository } = this.props
    if (ticketRepository) {
      await ticketRepository.onGetTicketDetails(id)
      if (!ticketRepository.getTicketDetailsErrors.length) {
        this.onGetTicketDetailsSuccess()
        this.onGetTicketInfoDropdownValues()
      }
    }
  }

  @action
  onGetTicketDetailsSuccess() {
    const { ticketRepository } = this.props
    if (ticketRepository && ticketRepository.ticketDetails.tags) {
      this.ticketTags = ticketRepository.ticketDetails.tags
    }
  }

  @action.bound
  async onGetTicketInfoDropdownValues() {
    const { ticketRepository } = this.props
    this.isGettingTicketInfoDropdownValues = true
    await ticketRepository.getTicketPriorities()
    this.onGetTicketInfoDropdownValuesSuccess()
  }

  @action.bound
  async onGetTicketInfoDropdownValuesSuccess() {
    this.isGettingTicketInfoDropdownValues = false
    const { ticketRepository } = this.props
    this.ticketPriorities = ticketRepository.ticketPriorities
  }

  @action.bound
  toggleDeviceDetailsModal(mode: string = 'viewMode') {
    this.isOpenDeviceDetailsModal = !this.isOpenDeviceDetailsModal
    this.deviceDetailsMode = mode
  }

  @action.bound
  toggleBasicInfoModal() {
    this.isOpenBasicInfoModal = !this.isOpenBasicInfoModal
  }

  @autobind
  renderAccordionHeader(text: string) {
    return <span>{text}</span>
  }

  renderValueField(field: string, value: any) {
    const valueString = value || ''
    return (
      <Row className="detail-row">
        <Col xs={5} className="text-right detail-label">
          {field}:
        </Col>
        <Col xs={7} className="detail-value">
          {valueString}
        </Col>
      </Row>
    )
  }

  renderTicketInfo() {
    const { ticketRepository = {} } = this.props
    const {
      ticketDetails = {},
      onEditTicketStatus,
      isEditingTicketStatus,
      onEditTicketDetail,
      isEdittingTicketDetails,
      ticketStatuses,
    } = ticketRepository
    const { id, ticketId, status, priorityId, ticketQueueId } = ticketDetails
    return (
      <CollapsibleWidget toggleButton={this.renderAccordionHeader('Ticket Info')}>
        <ContentContainer>
          <div>
            {this.renderValueField('Ticket Id', ticketId && ticketId.length ? ticketId : id)}
            {this.renderValueField(
              'Status',
              <SelectDropdown
                value={status}
                options={ticketStatuses}
                name="ticketStatus"
                disabled={isEditingTicketStatus}
                onChange={event => {
                  onEditTicketStatus(ticketId, event.target.value)
                  this.onGetTicketInfoDropdownValues()
                }}
              />
            )}
            {this.renderValueField(
              'Priority',
              <SelectDropdown
                value={priorityId || ''}
                options={this.ticketPriorities}
                name="ticketPriority"
                disabled={isEdittingTicketDetails.priorityId}
                onChange={event => {
                  onEditTicketDetail({
                    ticketId: ticketId,
                    key: 'priorityId',
                    value: Number(event.target.value),
                  })
                }}
              />
            )}
          </div>
        </ContentContainer>
      </CollapsibleWidget>
    )
  }

  renderPeople() {
    const { ticketRepository = {} } = this.props
    const { ticketDetails = {} } = ticketRepository
    const { owner = {}, requestor = {}, otherOwner = {} } = ticketDetails
    return (
      <CollapsibleWidget toggleButton={this.renderAccordionHeader('People')}>
        <ContentContainer>
          <div>
            {this.renderValueField('Primary Owner', <Link to={TEMP_REDIRECT}>Susan Boyle</Link>)}
            {this.renderValueField('Other Owner', <Link to={TEMP_REDIRECT}>P. Ness</Link>)}
            {this.renderValueField('Requestor', <Link to={TEMP_REDIRECT}>Julian Hohendoff</Link>)}
            {this.renderValueField('Cc', <Link to={TEMP_REDIRECT}>Click to add</Link>)}
            {this.renderValueField('AdminCc', '')}
          </div>
        </ContentContainer>
      </CollapsibleWidget>
    )
  }

  renderBasic() {
    const { ticketRepository = {} } = this.props
    const { ticketDetails = {} } = ticketRepository
    const { country, pointOfPurchase, inquiryType } = ticketDetails
    return (
      <CollapsibleWidget
        toggleButton={this.renderAccordionHeader('Basic')}
        hasEdit={true}
        editAction={this.toggleBasicInfoModal}
      >
        <ContentContainer>
          <div>
            {this.renderValueField('Inquiry Type', inquiryType && inquiryType.description)}
            {this.renderValueField('Country', country)}
            {this.renderValueField('PoP', pointOfPurchase)}
            {this.renderValueField('Forum URL', <Link to={TEMP_REDIRECT}>Link</Link>)}
          </div>
        </ContentContainer>
      </CollapsibleWidget>
    )
  }

  renderDeviceInfo() {
    const { ticketRepository = {} } = this.props
    const { ticketDetails = {} } = ticketRepository
    const { deviceDetails } = ticketDetails
    return (
      <CollapsibleWidget
        toggleButton={this.renderAccordionHeader('Device Info')}
        hasEdit={true}
        editAction={() => this.toggleDeviceDetailsModal('editMode')}
      >
        <ContentContainer>
          {deviceDetails ? this.renderValueField('S/N', `${deviceDetails.length} Device(s)`) : 'No Device Details'}
          {deviceDetails ? (
            <Row className="detail-row">
              <Col xs={5} className="text-right detail-label">
                {' '}
              </Col>
              <Col xs={7} className="detail-value">
                <a href="javascript:void(0)" onClick={() => this.toggleDeviceDetailsModal('viewMode')}>
                  View
                </a>
              </Col>
            </Row>
          ) : null}
        </ContentContainer>
      </CollapsibleWidget>
    )
  }

  renderDates() {
    const { ticketRepository = {} } = this.props
    const { ticketDetails = {} } = ticketRepository
    const { creationDate, startedDate, lastContactDate, dueDate, lastUpdateDate, closedDate } = ticketDetails

    return (
      <CollapsibleWidget toggleButton={this.renderAccordionHeader('Dates')}>
        <ContentContainer>
          <div>
            {this.renderValueField('Created', getFormattedDateString(creationDate, ''))}
            {this.renderValueField('Opened', getFormattedDateString(startedDate, ''))}
            {this.renderValueField('Last Updated', getFormattedDateString(lastUpdateDate, ''))}
            {this.renderValueField('Last Contact', getFormattedDateString(lastContactDate, ''))}
            {this.renderValueField('Due', getFormattedDateString(dueDate, ''))}
            {this.renderValueField('Closed', getFormattedDateString(closedDate, ''))}
            {this.renderValueField('Updated', getFormattedDateString(lastUpdateDate, ''))}
          </div>
        </ContentContainer>
      </CollapsibleWidget>
    )
  }

  renderTagsInputForm() {
    return (
      <CollapsibleWidget toggleButton={this.renderAccordionHeader('Tags')}>
        <ContentContainer>
          <TagsInputForm tags={this.ticketTags} onChangeTags={this.onChangeTags} />
        </ContentContainer>
      </CollapsibleWidget>
    )
  }

  renderAttachments() {
    const {
      onGetTicketAttachments,
      isLoadingAttachments,
      attachments,
      loadingAttachmentsFailed,
    } = this.props.ticketRepository
    return (
      <CollapsibleWidget toggleButton={this.renderAccordionHeader('Attachments')}>
        <ContentContainer>
          <TicketAttachments
            loadingAttachmentsFailed={loadingAttachmentsFailed}
            attachments={attachments}
            onGetTicketAttachments={onGetTicketAttachments}
            isLoadingAttachments={isLoadingAttachments}
            ticketId={this.props.ticketId}
          />
        </ContentContainer>
      </CollapsibleWidget>
    )
  }

  renderLoadingPanel() {
    return (
      <div className="text-center">
        <Icon xlarge iconName="spinner fa-spin" />
      </div>
    )
  }

  renderErrorMessagePanel(errorMessage: Array<ApiError>) {
    return (
      <StyledErrorMessage>
        <div className="error-message">{errorMessage.map(item => item.message)}</div>

        <Button onClick={() => this.onGetTicketDetails(this.props.ticketId)} bsStyle="primary">
          Try Again
        </Button>
      </StyledErrorMessage>
    )
  }

  renderTicketDetails(getTicketDetailsErrors: Array<ApiError>) {
    return getTicketDetailsErrors.length > 0 ? (
      this.renderErrorMessagePanel(getTicketDetailsErrors)
    ) : (
      <div>
        {this.renderTicketInfo()}
        {this.renderPeople()}
        {this.renderBasic()}
        {this.renderDeviceInfo()}
        {this.renderTagsInputForm()}
        {this.renderDates()}
        {this.renderAttachments()}
      </div>
    )
  }
  render() {
    const { ticketRepository = {} } = this.props
    const { getTicketDetailsErrors, isGettingTicketDetails } = ticketRepository
    return (
      <StyledSidebarContent>
        {isGettingTicketDetails ? this.renderLoadingPanel() : this.renderTicketDetails(getTicketDetailsErrors)}
        {this.isOpenDeviceDetailsModal ? (
          <DeviceDetails toggleModal={this.toggleDeviceDetailsModal} mode={this.deviceDetailsMode} />
        ) : null}
        {this.isOpenBasicInfoModal ? <BasicInfo toggleModal={this.toggleBasicInfoModal} /> : null}
      </StyledSidebarContent>
    )
  }
}
