/* @flow */

import { action, observable } from 'mobx'
import { Button, Col, ControlLabel, Form, FormControl, FormGroup, Row } from 'react-bootstrap'
import { inject, observer } from 'mobx-react'
import { mergeTicket } from './services/'
import _ from 'lodash'
import ErrorsList from '../common/ErrorsList'
import Modal from '../common/Modal'
import React, { Component } from 'react'
import styled from 'styled-components'
import TicketRepository from './TicketRepository'

const SplitTicketContainer = styled.div`
  label {
    text-align: left;
  }
  .wysiwyg-wrapper {
    padding: 0;
  }

  .nticket-id {
    padding-top: 7px !important;
  }
`

const SelectionContainer = styled.div`
  width: 760px;
  margin: 20px auto;
`

const ColBoxContainer = styled.div`
  display: inline-block;
  margin: 0 15px;
`

const ColSelection = styled.div`
  min-height: 300px;
  max-height: 450px;
  overflow: hidden;
  overflow-y: scroll;
  border: 1px solid #ccc;
  width: 350px;
  display: inline-block;
  margin: 0;
  padding: 5px;
`

type Props = {
  ticketRepository: TicketRepository,
  toggleModal: () => void,
  renderFields: () => void,
}

@inject('ticketRepository')
@observer
export default class SplitTicketFormModal extends Component {
  props: Props

  render() {
    const {
      toggleModal,
      show,
      renderFields,
      currStep,
      nextStep,
      prevStep,
      handleSubmit,
      hasError,
      ticketRepository,
      isSplittingTicket,
      isUploadingAttachments,
    } = this.props
    const { id, otherOwnerIds, deviceDetails } = ticketRepository.ticketDetails

    const { toggleSplitTicketOwnerIds, toggleSplitTicketSerialNumbers } = ticketRepository
    return (
      <Modal
        footer={
          <div>
            <Button
              bsSize="small"
              bsStyle="primary"
              onClick={currStep === 3 ? handleSubmit : nextStep}
              disabled={
                (currStep === 3 && _.filter(hasError, e => e !== null).length) ||
                isSplittingTicket ||
                isUploadingAttachments
              }
            >
              {currStep === 3 ? 'Submit' : isSplittingTicket ? 'Submitting...' : 'Next'}
            </Button>{' '}
            <Button bsSize="small" onClick={prevStep} disabled={isSplittingTicket || isUploadingAttachments}>
              Back
            </Button>
          </div>
        }
        showModal={this.props.show}
        onHide={toggleModal}
        bsSize="large"
      >
        <SplitTicketContainer>
          {currStep === 2 ? (
            <Col lg={12}>
              <h5>Select details to copy</h5>
              <SelectionContainer>
                <ColBoxContainer>
                  <h5>Other Owners</h5>
                  <ColSelection lg={5}>
                    {_.size(otherOwnerIds) ? (
                      otherOwnerIds.map(o => {
                        return (
                          <div>
                            <label>
                              <input
                                type="checkbox"
                                onChange={e => {
                                  toggleSplitTicketOwnerIds(e.target.checked, o.id)
                                }}
                              />{' '}
                              {o.email}
                            </label>
                          </div>
                        )
                      })
                    ) : (
                      <span>- No User(s) Available - </span>
                    )}
                  </ColSelection>
                </ColBoxContainer>
                <ColBoxContainer>
                  <h5>S/N</h5>
                  <ColSelection lg={5}>
                    {_.size(deviceDetails) ? (
                      deviceDetails.map(sn => {
                        return (
                          <div>
                            <label>
                              <input
                                type="checkbox"
                                onChange={e => {
                                  toggleSplitTicketSerialNumbers(e.target.checked, sn.serialNumber)
                                }}
                              />{' '}
                              {sn.serialNumber}
                            </label>
                          </div>
                        )
                      })
                    ) : (
                      <span>- No S/N Available - </span>
                    )}
                  </ColSelection>
                </ColBoxContainer>
              </SelectionContainer>
            </Col>
          ) : currStep === 3 ? (
            <Col lg={12}>
              <h5>Set new ticket details</h5>
              <Form horizontal>{renderFields()}</Form>
            </Col>
          ) : null}
        </SplitTicketContainer>
      </Modal>
    )
  }
}
