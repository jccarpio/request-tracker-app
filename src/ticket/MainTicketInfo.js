/* @flow */

import { Col, Row } from 'react-bootstrap'
import { getFormattedDateTimeString } from '../Helpers/dateFormatters'
import { inject, observer } from 'mobx-react'
import Avatar from '../common/Avatar'
import MessageContainer from '../common/MessageContainer'
import React, { Component } from 'react'
import styled from 'styled-components'
import TicketRepository from './TicketRepository'

const imageCat = 'https://pbs.twimg.com/profile_images/3471531856/11553d10c02660a214c876be65bf0401.jpeg'

const StyledMainTicketInfoContainer = styled.div`
  margin: 0;
  padding: 15px 0;
  .avatar-container {
    width: 90px;
  }
  .avatar {
    text-align: right;
  }
  .ticket-message-container {
    padding: 0 10px;
    box-sizing: border-box;
    width: ${props => (props.opensplit ? 'calc(100% - 140px)' : 'calc(100% - 90px)')};
  }
  .conversation-item {
    padding: 0;
  }
`

type Props = {
  isSplitPopupOpen: boolean,
  ticketRepository: TicketRepository,
}

const DUMMY_TEXT = 'Susan Boyle'

@inject('ticketRepository')
@observer
export default class MainTicketInfo extends Component<Props> {
  props: Props
  render() {
    const { isSplitPopupOpen, ticketRepository } = this.props
    const { getTicketDetailsErrors, isGettingTicketDetails } = ticketRepository
    const { name, creationDate, subject, inquiryDetail, createdBy, lastUpdateDate } = ticketRepository.ticketDetails
    return isGettingTicketDetails || getTicketDetailsErrors.length ? (
      <div />
    ) : (
      <StyledMainTicketInfoContainer opensplit={isSplitPopupOpen}>
        <Row className="conversation-item">
          <Col md={1} lg={1} />
          <Col md={1} lg={1} className="pull-left">
            <Avatar imageUrl={imageCat} onClick={() => alert('Avatar was clicked')} />
          </Col>
          <Col md={9} lg={9} className="mid-content pull-left">
            <MessageContainer
              author={name}
              ownerRole="ROLE_ADMIN"
              repliedTimeStamp={`Reported at ${getFormattedDateTimeString(lastUpdateDate)}`}
              creator={createdBy || DUMMY_TEXT}
              createdTimeStamp={getFormattedDateTimeString(creationDate)}
              title={subject}
            >
              {inquiryDetail ? <div dangerouslySetInnerHTML={{ __html: inquiryDetail }} /> : ''}
            </MessageContainer>
          </Col>
          <Col md={1} lg={1} className="pull-left" />
        </Row>
      </StyledMainTicketInfoContainer>
    )
  }
}
