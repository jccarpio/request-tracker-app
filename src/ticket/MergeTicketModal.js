/* @flow */

import { action, observable } from 'mobx'
import { Button, Col, ControlLabel, Form, FormControl, FormGroup } from 'react-bootstrap'
import { inject, observer } from 'mobx-react'
import { mergeTicket } from './services/'
import ErrorsList from '../common/ErrorsList'
import Modal from '../common/Modal'
import React, { Component } from 'react'
import styled from 'styled-components'
import TicketRepository from './TicketRepository'

const StyledCol = styled(Col)`
  margin: 0 0 20px 0;
`

type Props = {
  ticketRepository: TicketRepository,
  toggleModal: () => void,
}

@inject('ticketRepository')
@observer
export default class Ticket extends Component {
  props: Props

  @observable ticketId: string = ''
  @observable ticketIdToMerge: string = ''
  @observable isMergingTicket: boolean = false
  @observable apiErrors: ?Array<ApiError> = null

  @action.bound
  async handleSubmit(e: *) {
    const { ticketId } = this.props.ticketRepository.ticketDetails
    const { history } = this.props
    this.isMergingTicket = true
    const { data, errors } = await mergeTicket(ticketId, {
      ticketIdToMerge: this.ticketIdToMerge,
    })
    if (!errors.length) {
      alert('Ticket was successfully merged')
      history.push(`/ticket-conversation/${this.ticketIdToMerge}`)
      this.props.toggleModal()
    } else {
      this.apiErrors = errors
    }
    this.isMergingTicket = false
  }
  @action.bound
  handInputChange(e: *) {
    this.ticketIdToMerge = e.target.value
  }

  render() {
    const { toggleModal } = this.props
    const { ticketId } = this.props.ticketRepository.ticketDetails
    return (
      <Modal
        header={'Merge Tickets'}
        footer={
          <div>
            <Button
              bsSize="small"
              bsStyle="primary"
              onClick={this.handleSubmit}
              disabled={this.ticketIdToMerge == ticketId || this.isMergingTicket || !this.ticketIdToMerge}
            >
              {`${this.isMergingTicket ? 'Merging...' : 'Merge'}`}
            </Button>{' '}
            <Button bsSize="small" onClick={toggleModal} disabled={this.isMergingTicket}>
              Cancel
            </Button>
          </div>
        }
        showModal
        onHide={toggleModal}
      >
        <Form inline onSubmit={this.handleSubmit}>
          {this.apiErrors && (
            <StyledCol>
              <ErrorsList errors={this.apiErrors} />
            </StyledCol>
          )}
          <FormGroup>
            <ControlLabel>Merge Ticket #{ticketId} to</ControlLabel>{' '}
            <FormControl type="text" placeholder="Enter ticket number" onChange={this.handInputChange} />
          </FormGroup>
        </Form>
      </Modal>
    )
  }
}
