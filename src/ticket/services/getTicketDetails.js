/* @flow */

import { get } from '../../Helpers/API'

export default (ticketId: number) => {
  return get(`/api/tickets/${ticketId}`)
}
