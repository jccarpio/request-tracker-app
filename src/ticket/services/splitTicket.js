/* @flow */

import { update } from '../../Helpers/API'

export default (ticketId: any, payload: object) => {
  return update(`/api/tickets/split/${ticketId}`, payload)
}
