/* @flow */

import { get } from '../../Helpers/API'

export default (statusId: string) => {
  return get(`/api/tickets/status/${statusId}`)
}
