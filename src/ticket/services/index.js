import addTicketMessage from './addTicketMessage'
import createTicket from './createTicket'
import deleteTicketMessage from './deleteTicketMessage'
import editTicketDetails from './editTicketDetails'
import editTicketStatus from './editTicketStatus'
import getInquiryTypes from './getInquiryTypes'
import getTicketAttachments from './getTicketAttachments'
import getTicketDetails from './getTicketDetails'
import getTicketMessages from './getTicketMessages'
import getTicketPriorities from './getTicketPriorities'
import getTicketQueues from './getTicketQueues'
import getTicketStatuses from './getTicketStatuses'
import mergeTicket from './mergeTicket'
import splitTicket from './splitTicket'

export {
  createTicket,
  getTicketDetails,
  getTicketMessages,
  editTicketStatus,
  editTicketDetails,
  deleteTicketMessage,
  addTicketMessage,
  mergeTicket,
  getTicketQueues,
  getTicketPriorities,
  getTicketStatuses,
  splitTicket,
  getTicketAttachments,
  getInquiryTypes,
}
