/* @flow */

import { get } from '../../Helpers/API'

export default (ticketId: number, page: number = 1, userId?: number) => {
  const queryParams = `?page=${page}&size=5&sort=id,desc`
  return get(`/api/ticketMessages/ticket/${ticketId}${userId ? '/' + userId : ''}${queryParams}`)
}
