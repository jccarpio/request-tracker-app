/* @flow */

import { patch } from '../../Helpers/API'

export default (ticketId: string, statusId: string) => {
  return patch(`/api/tickets/status/${ticketId}`, { camundaOperation: statusId })
}
