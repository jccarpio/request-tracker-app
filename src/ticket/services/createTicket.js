/* @flow */

import { post } from '../../Helpers/API'
import _ from 'lodash'
export default (data: *) => {
  const additionalParams = {
    camundaOperation: 'NEW',
    version: 0,
  }
  const formValuePayload = { ...data, ...additionalParams }

  // let payload = new FormData()
  // _.each(formValuePayload, (formVal, key) => {
  //   if (key === 'attachmentFiles') {
  //     _.each(formVal, attachment => {
  //       payload.append(key, attachment.fileObject)
  //     })
  //   } else {
  //     payload.append(key, formVal)
  //   }
  // })

  return post(
    '/api/tickets',
    formValuePayload
    // {
    //   headers: {
    //     'Content-Type': 'multipart/form-data',
    //   },
    // }
  )
}
