/* @flow */

import { del } from '../../Helpers/API'

export default (ticketMessageId: number) => {
  return del(`/api/ticketMessages/${ticketMessageId}`)
}
