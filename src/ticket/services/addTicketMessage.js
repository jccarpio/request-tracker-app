/* @flow */

import { post } from '../../Helpers/API'

export default (messagePayload: { ticketId: number, userId: number, version: number, message: string }) => {
  return post(`/api/ticketMessages`, messagePayload)
}
