/* @flow */

import { get } from '../../Helpers/API'

export default (inquiryTypeId: string = '') => {
  return get(`/api/inquiryTypes/${inquiryTypeId}`)
}
