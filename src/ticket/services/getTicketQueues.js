/* @flow */

import { get } from '../../Helpers/API'

export default () => {
  return get(`/api/ticketQueues`)
}
