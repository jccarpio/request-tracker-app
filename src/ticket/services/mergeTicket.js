/* @flow */

import { update } from '../../Helpers/API'

export default (ticketId: number, payload: object) => {
  return update(`/api/tickets/merge/${ticketId}`, payload)
}
