/* @flow */

import { patch } from '../../Helpers/API'

export default (ticketId: string, payload: Object) => {
  return patch(`api/tickets/${ticketId}`, payload)
}
