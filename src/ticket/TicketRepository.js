/* @flow */

import { action, computed, observable, toJS } from 'mobx'
import {
  addTicketMessage,
  deleteTicketMessage,
  editTicketDetails,
  editTicketStatus,
  getInquiryTypes,
  getTicketAttachments,
  getTicketDetails,
  getTicketMessages,
  getTicketPriorities,
  getTicketQueues,
  getTicketStatuses,
} from './services'
import { type ApiError, apiHasErrors, cancelRequest } from '../Helpers/API'
import { confirmAlert } from 'react-confirm-alert'
import { notify } from 'react-notify-toast'
import { type TicketAttachment } from './types'
import { type TicketDetails } from './types'
import _ from 'lodash'
import listToOrderedMap from '../Helpers/listToOrderedMap'
import UserRepository from '../main/UserRepository'

const TICKET_INDEX_LABELS = {
  priorityId: 'Priority',
  ticketQueueId: 'Queue',
}

const DEFAULT_TICKET_DETAILS = {
  closedDate: '',
  creationDate: '',
  firmwareVersion: '',
  splitTicketId: 0,
  inquiryDetail: '',
  startedDate: '',
  lastUpdateDate: '',
  owner: {},
  pointOfPurchase: '',
  requestor: {},
  ticketId: '',
  mergeTicketId: null,
  tags: [],
  forumURL: '',
  country: '',
  attachments: [],
  email: '',
  previousStatus: '',
  dueDate: '',
  subject: '',
  serialNumber: '',
  attachmentFiles: [],
  ticketQueue: {},
  inquiryTypeId: null,
  lastContactDate: '',
  inquiryType: {},
  ticketQueueId: null,
  status: '',
  name: '',
  priority: '',
  lastUpdateBy: null,
  createdBy: null,
  id: 0,
  version: 0,
  otherOwner: {},
}

const DEFAULT_SPLIT_TICKET_DATA = {
  ticket: {
    priorityId: '',
    ticketQueueId: '',
    inquiryDetail: '',
    attachmentUniqueId: '',
  },
  ticketMessageIds: [],
  serialNumbers: [],
  otherOwnerIds: [],
}

type ApiErrors = Array<ApiError>

export default class TicketRepository {
  user: UserRepository
  constructor(user: UserRepository) {
    this.user = user
  }

  @observable notify = notify.createShowQueue()
  @observable currentTicketId: number = 1
  @observable ticketDetails: TicketDetails = DEFAULT_TICKET_DETAILS
  @observable isGettingTicketDetails: boolean = false
  @observable getTicketDetailsErrors: ApiErrors = []
  @observable getTicketMessageErrors: ApiErrors = []
  @observable ticketMessages: Object = {}
  @observable isGettingTicketMessage: boolean = false
  @observable messagesNextPage: number = 1
  @observable isLastPage: number = 1
  @observable totalElements: number = 0
  @observable isSendingMessage: boolean = false
  @observable splitTicketPayload: Object = DEFAULT_SPLIT_TICKET_DATA
  @observable selectedSplitTickets: Array<number> = []
  @observable selectedSplitTicketOwners: Array<object> = []
  @observable selectedSplitTicketSerials: Array<object> = []
  @observable ticketStatuses: Array<string> = []
  @observable isLoadingAttachments: boolean = false
  @observable attachments: Array<TicketAttachment> = []
  @observable loadingAttachmentsFailed: boolean = false
  @observable ticketPriorities: Array<object> = []
  @observable ticketQueues: Array<object> = []
  @observable inquiryTypes: Array<InquiryTypes> = []

  @action.bound
  onSetCurrentTicketId(id: number) {
    this.currentTicketId = id
  }

  @observable isEditingTicketStatus: boolean = false
  @observable editTicketStatusErrors: ApiErrors = []

  @observable isEdittingTicketDetails: { priorityId?: boolean, ticketQueueId?: boolean } = {}
  @observable editTicketDetailsErrors: { priorityId?: ApiErrors, ticketQueueId?: ApiErrors } = {}

  @action.bound
  async onGetTicketDetails(id: number, isRefresh?: boolean) {
    this.isGettingTicketDetails = true
    this.getTicketDetailsErrors = []
    if (!isRefresh) {
      this.clearTicketDetails()
    }

    const { data, errors } = await getTicketDetails(id)
    this.isGettingTicketDetails = false

    if (apiHasErrors({ data, errors })) {
      this.getTicketDetailsErrors = errors

      return
    }
    this.onGetTicketStatuses(data.status)
    this.ticketDetails = data
  }

  @action.bound
  clearTicketDetails() {
    this.ticketDetails = DEFAULT_TICKET_DETAILS
  }

  @action.bound
  clearTicketMessages() {
    this.ticketMessages = {}
    this.isLastPage = 1
    this.messagesNextPage = 1
    this.totalElements = 0
  }

  @action.bound
  async onGetTicketStatuses(statusId: string) {
    this.ticketStatuses = []

    const { data, errors } = await getTicketStatuses(statusId)
    if (!apiHasErrors({ data, errors })) {
      this.ticketStatuses = data.map(status => {
        return {
          text: status,
          value: status,
        }
      })
      if (data.indexOf(statusId) === -1) {
        this.ticketStatuses.push({
          text: statusId,
          value: statusId,
        })
      }
    }
  }

  @action.bound
  async onGetTicketMessages(id: number) {
    this.isGettingTicketMessage = true
    this.getTicketMessageErrors = []
    this.clearTicketMessages()
    const { data, errors } = await getTicketMessages(id, 1, this.user.currentUserId)

    this.isGettingTicketMessage = false

    if (apiHasErrors({ data, errors })) {
      this.getTicketMessageErrors = errors
      return
    }

    this.onGetMessageSuccess(data)
  }

  @action.bound
  onGetMessageSuccess(data: { content: Array<TicketDetails>, totalElements: number, last: number }) {
    this.getTicketMessageErrors = []
    this.ticketMessages = listToOrderedMap(data.content)

    this.isLastPage = data.last
    this.messagesNextPage = 2
    this.totalElements = data.totalElements
  }

  @computed
  get remainingMessages(): number {
    return this.totalElements - _.values(this.ticketMessages).length
  }

  @action.bound
  onGetTicketMessagesSuccess(data: Object) {
    const newTicketMessages = { ...listToOrderedMap(data.content), ...this.ticketMessages }
    this.ticketMessages = newTicketMessages
    this.isLastPage = data.last
    this.totalElements = data.totalElements
    this.messagesNextPage = data.last ? this.messagesNextPage : this.messagesNextPage + 1
  }

  @action.bound
  async onShowPreviousMessage(id: number) {
    if (this.isLastPage) {
      return
    }

    this.isGettingTicketMessage = true
    const { data, errors } = await getTicketMessages(id, this.messagesNextPage, this.user.currentUserId)

    this.isGettingTicketMessage = false

    if (apiHasErrors({ data, errors })) {
      alert(errors[0].message)
      return
    }

    this.onGetTicketMessagesSuccess(data)
  }

  @action.bound
  async onChangeTags(ticketId: number, tags: Array<string>) {
    await cancelRequest()
    editTicketDetails(ticketId, { tags })
  }

  @action.bound
  async onEditTicketStatus(ticketId: number, statusId: string) {
    this.editTicketStatusErrors = []
    this.isEditingTicketStatus = true
    const { data, errors } = await editTicketStatus(ticketId, statusId)
    this.isEditingTicketStatus = false
    if (apiHasErrors({ data, errors })) {
      this.notify(`Failed to update Status, Error: ${errors[0].message}`, 'error')
      return
    }
    this.onEditTicketStatusSuccess(statusId)
  }

  @action
  onEditTicketStatusFailed(errors: ApiErrors) {
    this.editTicketStatusErrors = errors
    alert(errors[0].message)
  }

  @action
  onEditTicketStatusSuccess(statusId: string) {
    this.ticketDetails.status = statusId
    this.notify(`Status now changed to ${statusId}`, 'success')
    this.onGetTicketStatuses(statusId)
  }

  @action.bound
  async onEditTicketDetail({ ticketId, key, value }: { ticketId: string, key: string, value: any }) {
    this.setEditTicketDetailsErrors(key, [])
    this.setEditTicketDetailsLoadingStatus(key, true)
    const { data, errors } = await editTicketDetails(ticketId, { [key]: value })

    this.setEditTicketDetailsLoadingStatus(key, false)
    if (apiHasErrors({ data, errors })) {
      this.onEditTicketDetailFailed(key, errors)
      return
    }
    this.onEditTicketDetailSuccess(key, value)
  }

  @action
  onEditTicketDetailFailed(key: string, errors: ApiErrors) {
    this.setEditTicketDetailsErrors(key, errors)
    this.notify(`Failed to update ${TICKET_INDEX_LABELS[key]}, Error: ${errors[0].message}`, 'error')
    // alert(errors[0].message)
  }

  @action
  onEditTicketDetailSuccess(key: string, value: any) {
    this.ticketDetails = {
      ...this.ticketDetails,
      [key]: value,
    }
    const valChanged = this.getTicketValueLabel(key, value)
    this.notify(`${TICKET_INDEX_LABELS[key]} now changed to ${valChanged}`, 'success')
  }

  @action.bound
  async getTicketPriorities() {
    const { data, errors } = await getTicketPriorities()
    if (!errors.length) {
      this.ticketPriorities = data.map(priority => {
        return { text: priority.description, value: priority.id }
      })
    } else this.notify(`Failed to fetch Ticket Priorities, Error: ${errors[0].message}`, 'error')
  }

  @action.bound
  async getTicketQueues() {
    const { data, errors } = await getTicketQueues()
    if (!errors.length) {
      this.ticketQueues = data.map(ticketQueue => {
        return { text: ticketQueue.description, value: ticketQueue.id }
      })
    } else this.notify(`Failed to fetch Ticket Queues, Error: ${errors[0].message}`, 'error')
  }

  @action
  getTicketValueLabel(key, value) {
    if (key === 'priorityId' && value) {
      return _.find(this.ticketPriorities, { value: parseInt(value) }).text
    } else if (key === 'ticketQueueId' && value) {
      return _.find(this.ticketQueues, { value: parseInt(value) }).text
    }
  }

  @action
  setEditTicketDetailsErrors(key: string, errors: ApiErrors) {
    this.editTicketDetailsErrors = {
      ...this.editTicketDetailsErrors,
      [key]: errors,
    }
  }

  @action
  setEditTicketDetailsLoadingStatus(key: string, value: boolean) {
    this.isEdittingTicketDetails = {
      ...this.isEdittingTicketDetails,
      [key]: value,
    }
  }

  @action.bound
  async onDeleteMessagePressed(ticketMessageId: number, ticketId: number) {
    return confirmAlert({
      title: 'Delete Message',
      message: 'Are you sure want to delete this message',
      buttons: [
        {
          label: 'Confirm Delete',
          onClick: () => this.onDeleteMessage(ticketMessageId, ticketId),
        },
        {
          label: 'Cancel',
          onClick: () => null,
        },
      ],
    })
  }

  @action.bound
  async onDeleteMessage(ticketMessageId: number, ticketId: number) {
    this.setMessageLoadingStatus(ticketMessageId, true)
    const { data, errors } = await deleteTicketMessage(ticketMessageId)
    this.setMessageLoadingStatus(ticketMessageId, false)
    if (apiHasErrors({ data, errors })) {
      alert(errors[0].message)
      return
    }

    this.removeTicketMessageById(ticketMessageId)
    this.onGetTicketAttachments(ticketId)
  }

  @action.bound
  removeTicketMessageById(ticketMessageId: number) {
    this.ticketMessages = _.omit(this.ticketMessages, [ticketMessageId])
  }

  @action.bound
  setMessageLoadingStatus(id: number, loadingStatus: boolean) {
    const clonedTicketMessages = _.clone(toJS(this.ticketMessages))
    this.ticketMessages = _.set(clonedTicketMessages, [id, 'isLoading'], loadingStatus)
  }

  @computed
  get sortedTicketMessages(): Object {
    return _.sortBy(this.ticketMessages, ['id'])
  }

  @action.bound
  async onAddTicketMessage(ticketId: number, replyFormValues: Object) {
    this.isSendingMessage = true

    const { data, errors } = await addTicketMessage({
      ticketId: ticketId,
      userId: this.user.currentUserId,
      version: 0,
      ..._.mapValues(replyFormValues, d => d.value),
    })

    this.isSendingMessage = false

    if (apiHasErrors({ data, errors })) {
      alert(errors[0].message)
      return
    }

    this.ticketMessages = {
      ...this.ticketMessages,
      [data.id]: data,
    }

    this.onGetTicketAttachments(ticketId)
  }

  @action.bound
  async onGetTicketAttachments(ticketId: number) {
    this.isLoadingAttachments = true
    this.attachments = []
    const { data, errors } = await getTicketAttachments(ticketId)
    this.isLoadingAttachments = false
    if (apiHasErrors({ data, errors })) {
      this.onGetTicketAttachmentsFailed()
      return
    }
    this.onGetTicketAttachmentsSuccess(data)
  }

  @action
  onGetTicketAttachmentsFailed() {
    this.loadingAttachmentsFailed = true
  }

  @action
  onGetTicketAttachmentsSuccess(data: Array<TicketAttachment>) {
    this.attachments = data
  }

  @action.bound
  toggleSplitTicketMessageIds(isChecked, value) {
    const { splitTicketPayload } = this
    const { ticketMessageIds } = splitTicketPayload

    if (isChecked) {
      ticketMessageIds.push(value)
    } else {
      ticketMessageIds.splice(ticketMessageIds.indexOf(value), 1)
    }
  }

  @action.bound
  toggleSplitTicketOwnerIds(isChecked, value) {
    const { splitTicketPayload } = this
    const { otherOwnerIds } = splitTicketPayload

    if (isChecked) {
      otherOwnerIds.push(value)
    } else {
      otherOwnerIds.splice(otherOwnerIds.indexOf(value), 1)
    }
  }

  @action.bound
  toggleSplitTicketSerialNumbers(isChecked, value) {
    const { splitTicketPayload } = this
    const { serialNumbers } = splitTicketPayload

    if (isChecked) {
      serialNumbers.push(value)
    } else {
      serialNumbers.splice(serialNumbers.indexOf(value), 1)
    }
  }

  @action.bound
  resetSplitTicketData() {
    this.splitTicketPayload = DEFAULT_SPLIT_TICKET_DATA
  }

  @action.bound
  async getInquiryTypes() {
    const { data, errors } = await getInquiryTypes()

    if (!errors.length) {
      this.inquiryTypes = _.map(data, d => ({
        value: d.id,
        label: d.description,
      }))
    }
  }
}
