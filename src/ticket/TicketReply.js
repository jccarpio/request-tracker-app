/* @flow */

import { action, computed, observable } from 'mobx'
import { autobind } from 'core-decorators'
import { Button, Icon, Image } from '../Styled'
import { Form, FormGroup } from 'react-bootstrap'
import { observer } from 'mobx-react'
import { required } from '../Helpers/Validations'
import { TicketReply, TicketReplyActions, TicketSplitMergeContainer } from './styled'
import React, { Component } from 'react'
import styled from 'styled-components'
import WysiwygEditor from '../common/WysiwygEditor'

const StyledRichTextEditor = styled.div`
  display: ${props => (props.isReplyOpen ? 'block' : 'none')};
  button {
    margin: 0 10px;
  }
`
const replyDefaultFormValues = {
  attachmentUniqueId: { value: '' },
  message: { value: '', hasChanged: false, hasText: false },
}

@observer
export default class TicketReplyComponent extends Component {
  @observable
  replyFormValues = {
    ...replyDefaultFormValues,
  }

  @observable isUploadingAttachments: boolean = false
  @observable attachmentsHasVirus: boolean = false
  richTextEditor: any
  @observable resetWysiwyg: boolean = false

  @computed
  get formErrors(): any {
    const { validateField } = this
    return {
      message: validateField('message'),
    }
  }

  @autobind
  validateField(fieldName: string): ?string {
    const { hasText } = this.replyFormValues[fieldName]

    return required(hasText)
  }

  @action.bound
  handleEditorChange(value: string, hasText: boolean) {
    this.replyFormValues.message.value = value
    this.replyFormValues.message.hasText = hasText
  }

  @action.bound
  handleEditorBlur(value: string, hasText: boolean) {
    this.replyFormValues.message = {
      value,
      hasText,
      hasChanged: true,
    }
  }

  @action.bound
  onAttachmentUploadStart() {
    this.isUploadingAttachments = true
  }

  @action.bound
  onAttachmentUploadFailed() {
    this.isUploadingAttachments = false
  }

  @action.bound
  handleAttachmentChange(attachmentUniqueId: string, hasVirus: boolean) {
    this.isUploadingAttachments = false
    this.attachmentsHasVirus = hasVirus
    this.replyFormValues.attachmentUniqueId = {
      value: attachmentUniqueId,
    }
  }

  @autobind
  resetForm() {
    this.props.onToggleReplyOpen()
    this.richTextEditor.clearEditorState()
  }

  render() {
    const { onToggleReplyOpen, isReplyOpen, onSubmitReply, onMergeClick, onSplitClick, isLoading } = this.props
    const { hasChanged } = this.replyFormValues['message']
    const error = this.formErrors['message']
    const showError = Boolean(error) && hasChanged
    const disableSubmit = isLoading || error || this.isUploadingAttachments || this.attachmentsHasVirus

    return (
      <TicketReply isReplyOpen={isReplyOpen}>
        <TicketSplitMergeContainer>
          <a href="javascript:void(0)" onClick={onMergeClick}>
            <Image src={require('../images/merge.png')} />
          </a>{' '}
          <a href="javascript:void(0)" onClick={onSplitClick}>
            <Image src={require('../images/split.png')} />
          </a>
        </TicketSplitMergeContainer>
        <TicketReplyActions>
          <a onClick={onToggleReplyOpen} className={`text-link reply${isReplyOpen && '-opened'}`}>
            {isReplyOpen && <Icon iconName="chevron-down" />} Reply
          </a>{' '}
          / <a className="text-link">Note</a>
        </TicketReplyActions>
        <StyledRichTextEditor isReplyOpen={isReplyOpen}>
          <Form onSubmit={() => onSubmitReply(this.replyFormValues, this.resetForm)}>
            <FormGroup className={`${showError ? 'has-error' : ''}`}>
              <WysiwygEditor
                onChange={this.handleEditorChange}
                onBlur={this.handleEditorBlur}
                textFieldHelpBlock={showError ? error : null}
                onAttachmentUploadStart={this.onAttachmentUploadStart}
                onAttachmentUploadComplete={this.handleAttachmentChange}
                onAttachmentUploadFailed={this.onAttachmentUploadFailed}
                ref={editor => {
                  this.richTextEditor = editor
                }}
              />
            </FormGroup>
            <div className="text-right" id="submit-reply">
              <Button type="submit" bsStyle="secondary" disabled={disableSubmit}>
                {isLoading ? `Sending Reply....` : 'Submit'}
              </Button>{' '}
              <Button type="button" onClick={this.resetForm} disabled={isLoading}>
                Cancel
              </Button>
            </div>
          </Form>
        </StyledRichTextEditor>
      </TicketReply>
    )
  }
}
