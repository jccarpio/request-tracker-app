/* @flow */

import { getFormattedDateTimeString } from '../Helpers/dateFormatters'
import { type TicketAttachment } from './types'
import React, { PureComponent } from 'react'
import styled from 'styled-components'

type Props = {
  data: TicketAttachment,
  hideSubInfo: boolean,
  showThumbnail?: boolean,
}

const StyledAttachmentContainer = styled.div`
  margin-bottom: 5px;
  margin: 0 15px;
  flex: 1;
  &:last-of-type {
    margin-bottom: 0;
  }
  .main-info {
    margin: 0;
    margintop: 10px;
  }
  .sub-info {
    font-size: 12px;
  }
  .atc-thumbnail {
    box-shadow: 2px 2px 3px rgba(0, 0, 0, 0.2);
    border: 1px solid rgba(0, 0, 0, 0.2);
    margin-bottom: 5px;
    margin-top: 10px;
    display: block;
  }
  .file-size {
    font-size: 10px;
  }
  &.attachment {
    max-width: 180px;
    .file-name {
      max-witdh: 180px;
      white-space: nowrap;
      overflow: hidden;
      text-overflow: ellipsis;
    }
  }
  &.sidebar-attachment {
    width: 100%;
    margin-bottom: 10px;
    .file-name {
      max-width: 100%;
      white-space: nowrap;
      overflow: hidden;
      text-overflow: ellipsis;
    }
  }
`

export default class Attachment extends PureComponent {
  props: Props
  render() {
    const { data, hideSubInfo, showThumbnail = false, className = '' } = this.props
    const {
      resourcePath,
      resourcePathThumbnail,
      fileName,
      fileSize,
      creationDate = '2018-03-10T03:00:00.000Z',
      owner = { name: 'Julian Hohendoff' },
    } = data
    return (
      <StyledAttachmentContainer className={`${className}attachment`}>
        <div>
          <p className="main-info">
            <div className="file-name">
              <a className="text-link" href={`/api${resourcePath}`} target="_blank" title={fileName}>
                {resourcePathThumbnail &&
                  showThumbnail && <img src={`/api${resourcePathThumbnail}`} className="atc-thumbnail" />}
                {fileName}
              </a>{' '}
            </div>
            <div className="file-size">({fileSize})</div>
          </p>
          {!hideSubInfo && (
            <span className="sub-info">
              {getFormattedDateTimeString(creationDate, '')} by{' '}
              <a href="#" className="text-link">
                {owner.name}
              </a>
            </span>
          )}
        </div>
      </StyledAttachmentContainer>
    )
  }
}
