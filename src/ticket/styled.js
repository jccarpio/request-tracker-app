import { Col, Row } from 'react-bootstrap'
import styled from 'styled-components'

export const TicketReply = styled.div`
  border-top: 1px solid #ccc;
  .reply-opened {
    color: #000;
    font-size: 18px;
  }

  #submit-reply {
    padding: 10px 20px;
  }
`
export const TicketReplyActions = styled.div`
  padding: 15px;
  color: ${props => (props.current ? '#333' : 'inherit')};
  font-size: ${props => (props.current ? '18px' : 'inherit')};
  a {
    cursor: pointer;
    &:hover {
      color: #2199e8 !important;
    }
  }
`
export const TicketConversationContainer = styled.div`
  // height: 95%;
  width: 100%;
  padding: 0 50px;
  margin-bottom: 20px;
`
export const TicketSplitMergeContainer = styled.div`
  padding: 5px 10px;
  position: absolute;
  right: 0;
  background: #20cb6a;
  border-top-left-radius: 10px;
  border-bottom-left-radius: 10px;
  top: -50px;
  text-align: center;
  a {
    img {
      height: 30px;
      width: 30px;
      -webkit-filter: invert(100%);
      filter: invert(100%);
    }
  }
`

export const AddDeviceAction = styled.a`
  margin: 10px 0;
  color: inherit;
  display: block;
  cursor: pointer;
  &:hover {
    color: inherit;
  }
`
export const PaddedCol = styled(Col)`
  padding: 5px 15px;
  .form-control {
    width: 100%;
  }
`
export const PaddedColAbsolute = styled(Col)`
  position: absolute;
  right: -15px;
  margin-top: 8px;
  a {
    display: block;
    padding: 6px;
    i.fa {
      &.text-danger {
        font-size: 18px;
      }
    }
  }
`
export const RelativeRow = styled(Row)`
  position: relatve;
`
