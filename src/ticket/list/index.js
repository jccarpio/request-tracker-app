/* @flow */

import { Panel as _Panel, Col, ControlLabel, Form, FormControl, FormGroup, Grid, Row, Table } from 'react-bootstrap'
import { action, observable } from 'mobx'
import { observer } from 'mobx-react'
import MainLayout from '../../Styled/MainLayout'
import React, { Component } from 'react'
import ReactTable from '../../common/ReactTable'
import styled from 'styled-components'
import TimeAgo from 'react-timeago'

const PageHeader = styled(Col)`
  margin-top: 20px;
  .panel-body {
    height: 100px;
    .avatar {
      display: inline-block;
      height: 80px;
      vertical-align: middle;
      img {
        vertical-align: middle;
      }
    }
    h2 {
      margin-left: 20px;
      display: inline-block;
    }
  }
`

const SearchForm = styled(Row)`
  padding: 20px 0;
  label {
    text-align: left !important;
  }
`

@observer
export default class TicketList extends Component {
  static ticketsOwnedColumDef: Array = [
    {
      Header: 'Number',
      accessor: 'ticketId',
    },
    {
      Header: 'Title',
      accessor: 'subject',
    },
    {
      Header: 'Queue',
      accessor: 'ticketQueue.description',
    },
    {
      Header: 'Status',
      accessor: 'status',
    },
    {
      Header: 'Created',
      accessor: 'creationDate',
      Cell: d => <TimeAgo date={new Date(d.row.creationDate)} />,
    },
    {
      Header: 'SLA',
      accessor: 'sla',
    },
  ]
  render() {
    return (
      <MainLayout>
        <Grid className="main-content">
          <Col lg={12}>
            <Row>
              <PageHeader lg={12}>
                <h4>Tickets</h4>
              </PageHeader>
            </Row>
            <SearchForm>
              <Col lg={12}>
                <Col lg={5}>
                  <Form horizontal>
                    <FormGroup>
                      <Col lg={3} componentClass={ControlLabel}>
                        Search
                      </Col>
                      <Col lg={9}>
                        <FormControl name="search" type="text" placeholder="Search tickets" />
                      </Col>
                    </FormGroup>
                    <FormGroup>
                      <Col lg={3} componentClass={ControlLabel}>
                        Filter By:
                      </Col>
                      <Col lg={9} />
                    </FormGroup>
                  </Form>
                </Col>
              </Col>
            </SearchForm>
            <Row>
              <Col lg={12}>
                <ReactTable
                  columnDefs={TicketList.ticketsOwnedColumDef}
                  defaultPageSize={10}
                  showPagination={false}
                  isServerSide={true}
                  endpoint="tickets"
                />
              </Col>
            </Row>
          </Col>
        </Grid>
      </MainLayout>
    )
  }
}
