/* @flow */

import { action, computed, observable } from 'mobx'
import { autobind } from 'core-decorators'
import { inject, observer } from 'mobx-react'
import MainLayout from '../Styled/MainLayout'
import MainTicketInfo from './MainTicketInfo'
import MergeTicketModal from './MergeTicketModal'
import React, { Component, type Node } from 'react'
import scrollToComponent from 'react-scroll-to-component'
import SplitTicketPopup from './SplitTicketPopup'
import styled from 'styled-components'
import TicketConversationSidebar from './TicketConversationSidebar'
import TicketConversationThread from './TicketConversationThread'
import TicketReply from './TicketReply'

const ThreadContainer = styled.div`
  padding-bottom: ${props => (props.padBottom ? props.padBottom : '60px')};
`

const BottomFixedContainer = styled.div`
  background: #fff;
  bottom: 0;
  position: fixed;
  z-index: 99;
  width: ${props => (props.width ? props.width : '100%')};
  left: ${props => (props.isSplitPopupOpen ? '0' : 'auto')};
  right: ${props => (props.isSplitPopupOpen ? '0' : 'auto')};
`

@inject('ticketRepository')
@observer
export default class TicketConversation extends Component<*> {
  bottomOfMessage: Node

  @observable isReplyOpen = false
  @observable isMergeModalOpen = false
  @observable isSplitPopupOpen = false
  @observable isSplitPopupFormOpen = false
  @observable bottomFixedContainerWidth = '100%'

  @computed
  get bottomFixedContainerHeight() {
    if (this.isReplyOpen) {
      return '450px'
    } else if (this.isSplitPopupFormOpen) {
      return '561px'
    } else if (this.isSplitPopupOpen) {
      return '86px'
    }
    return '60px'
  }

  componentDidMount() {
    window.addEventListener('resize', this.calculateTicketContainerWidth)

    this.calculateTicketContainerWidth()
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.calculateTicketContainerWidth)
  }

  @action.bound
  calculateTicketContainerWidth() {
    const containerEl = document.getElementById('ticket-container')
    if (containerEl) {
      const width = containerEl.clientWidth
      this.bottomFixedContainerWidth = `${width}px`
    }
  }

  @action.bound
  toggleMergeModal() {
    this.isMergeModalOpen = !this.isMergeModalOpen
  }

  @action.bound
  toggleReplyOpen() {
    this.isReplyOpen = !this.isReplyOpen
  }

  @action.bound
  toggleSplitPopUp() {
    this.isSplitPopupOpen = !this.isSplitPopupOpen
    if (this.isSplitPopupOpen) {
      this.isReplyOpen = false
    } else {
      this.isSplitPopupFormOpen = false
    }
  }

  @action.bound
  toggleSplitPopUpForm() {
    this.isSplitPopupFormOpen = !this.isSplitPopupFormOpen
  }

  @autobind
  async onSubmitReply(replyFormValues: any, resetForm: any) {
    await this.props.ticketRepository.onAddTicketMessage(this.ticketIdFromParams, replyFormValues)
    resetForm()
    this.isReplyOpen = false
    scrollToComponent(this.bottomOfMessage, {
      offset: -250,
      align: 'top',
      duration: 1500,
    })
  }

  renderTicketReply() {
    return (
      <TicketReply
        onToggleReplyOpen={this.toggleReplyOpen}
        isReplyOpen={this.isReplyOpen}
        onMergeClick={this.toggleMergeModal}
        onSplitClick={this.toggleSplitPopUp}
        ticketId={this.ticketIdFromParams}
        onSubmitReply={this.onSubmitReply}
        isLoading={this.props.ticketRepository.isSendingMessage}
      />
    )
  }

  renderMergeTicketModal() {
    return <MergeTicketModal showModal={this.isMergeModalOpen} toggleModal={this.toggleMergeModal} {...this.props} />
  }

  get ticketIdFromParams() {
    const ticketId = this.props.match && this.props.match.params && this.props.match.params.id
    return ticketId
  }

  render() {
    return (
      <MainLayout>
        <section className="main-container" id="main-container">
          <section className="sidebar">
            <TicketConversationSidebar ticketId={this.ticketIdFromParams} />
          </section>
          <section className="main-content" id="ticket-container">
            <MainTicketInfo />
            <ThreadContainer padBottom={this.bottomFixedContainerHeight}>
              <TicketConversationThread
                userId={this.props.ticketRepository.user.currentUserId}
                ticketId={this.ticketIdFromParams}
                isSplitPopupOpen={this.isSplitPopupOpen}
                // onReplyToMessageItem={this.onReplyToMessageItem}
              />
              <div
                ref={section => {
                  this.bottomOfMessage = section
                }}
              />
            </ThreadContainer>
            <BottomFixedContainer
              isSplitPopupOpen={this.isSplitPopupOpen}
              width={!this.isSplitPopupOpen && this.bottomFixedContainerWidth}
            >
              {!this.isSplitPopupOpen && this.renderTicketReply()}
              {this.isSplitPopupOpen && (
                <SplitTicketPopup
                  toggleForm={this.toggleSplitPopUpForm}
                  togglePopup={this.toggleSplitPopUp}
                  {...this.props}
                />
              )}
            </BottomFixedContainer>
            {this.isMergeModalOpen && this.renderMergeTicketModal()}
          </section>
        </section>
      </MainLayout>
    )
  }
}
