/* @flow */

import { action, observable } from 'mobx'
import { AddDeviceAction, PaddedCol, PaddedColAbsolute, RelativeRow } from '../styled'
import { Button, Col, ControlLabel, Form, FormControl, FormGroup, Row, Table } from 'react-bootstrap'
import { editTicketDetails } from '../services/'
import { Icon } from '../../Styled/'
import { inject, observer } from 'mobx-react'
import { injectGlobal } from 'styled-components'
import { mergeTicket } from '../services/'
import { notify } from 'react-notify-toast'
import _ from 'lodash'
import ErrorsList from '../../common/ErrorsList'
import Modal from '../../common/Modal'
import React, { Component } from 'react'
import TicketRepository from '../TicketRepository'

injectGlobal`
  .device-details-modal {
    .modal-body {
      max-height:390px;
      overflow:hidden;
      overflow-y:auto;
      p {
        margin:0 0 10px 0;
      }
    }
    .modal-footer {
      .row {
        position:relative;
        .row:first-child {
          text-align:left;
          padding: 0 30px;
          position: absolute;
          top: -30px;
        }
      }
    }
  }
`

const DEFAULT_DEVICE_DETAILS = {
  firmwareVersion: '',
  serialNumber: '',
}

type Props = {
  ticketRepository: TicketRepository,
  toggleModal: () => void,
}

@inject('ticketRepository')
@observer
export default class DeviceDetails extends Component {
  props: Props

  @observable notify = notify.createShowQueue()
  @observable isUpdating: boolean = false
  @observable apiErrors: ?Array<ApiError> = null
  @observable deviceDetails: Array<object> = [DEFAULT_DEVICE_DETAILS]

  @action.bound
  addDeviceInfoRow(e: *) {
    this.deviceDetails.push(DEFAULT_DEVICE_DETAILS)
  }

  @action.bound
  removeDeviceInfoRow(index: number) {
    this.deviceDetails.splice(index, 1)
  }

  @action.bound
  handleDeviceDetailsChange(index: number, field, value) {
    this.deviceDetails[index] = {
      ...this.deviceDetails[index],
      [field]: value,
    }
  }

  @action.bound
  async handleSubmit(e: any) {
    this.isUpdating = true
    let { ticketId, deviceDetails } = this.props.ticketRepository.ticketDetails
    const { data, errors } = await editTicketDetails(ticketId, {
      deviceDetails: _.filter(this.deviceDetails, d => !_.isEmpty(d.serialNumber)),
    })
    if (!errors.length) {
      this.notify(`Device Details is updated.`, 'success')
      this.props.ticketRepository.ticketDetails = data
      this.props.toggleModal()
    } else {
      this.apiErrors = errors
      this.notify(`Failed to Update Device Details, Error: ${errors[0].message}`, 'error')
    }
    this.isUpdating = false
  }

  componentWillMount() {
    const { deviceDetails } = this.props.ticketRepository.ticketDetails
    if (deviceDetails) {
      this.deviceDetails = [...deviceDetails]
    }
  }

  render() {
    const { toggleModal, mode } = this.props
    return (
      <Modal
        header={`${mode === 'editMode' ? 'Edit ' : ''}Device Details`}
        footer={
          <Row>
            <Row>
              {mode === 'editMode' ? (
                <Col lg={12}>
                  <AddDeviceAction herf="javascript:void(0)" onClick={this.addDeviceInfoRow}>
                    <Icon iconName="plus-circle" sClass="text-success" /> Add another device
                  </AddDeviceAction>
                </Col>
              ) : null}
            </Row>
            <Row>
              <Col lg={12}>
                {mode === 'editMode' ? (
                  <Button bsSize="small" bsStyle="primary" onClick={this.handleSubmit} disabled={this.isUpdating}>
                    {`${this.isUpdating ? 'Saving...' : 'Save'}`}
                  </Button>
                ) : null}
                <Button bsSize="small" onClick={toggleModal} disabled={this.isUpdating}>
                  Close
                </Button>
              </Col>
            </Row>
          </Row>
        }
        showModal
        onHide={toggleModal}
        className="device-details-modal"
      >
        {mode === 'editMode' ? (
          <Form inline onSubmit={this.handleSubmit}>
            <Col lg={12}>
              <p>{this.apiErrors && <ErrorsList errors={this.apiErrors} />}</p>
            </Col>
            <Col lg={12}>
              <Row>
                <Col lg={6} componentClass={ControlLabel}>
                  Firmware Version
                </Col>
                <Col lg={6} componentClass={ControlLabel}>
                  Serial Number
                </Col>
              </Row>
              {_.map(this.deviceDetails, (row, key) => {
                return (
                  <RelativeRow key={key}>
                    {_.map(row, (d, index) => {
                      return (
                        <PaddedCol sm={6} key={index}>
                          <FormControl
                            defaultValue={d}
                            name={index}
                            onChange={e => {
                              this.handleDeviceDetailsChange(key, index, e.target.value)
                            }}
                            onBlur={e => {
                              this.handleDeviceDetailsChange(key, index, e.target.value)
                            }}
                          />
                        </PaddedCol>
                      )
                    })}
                    {key > 0 && (
                      <PaddedColAbsolute>
                        <a
                          href="javascript:void(0)"
                          onClick={() => {
                            this.removeDeviceInfoRow(key)
                          }}
                        >
                          <Icon iconName="minus-circle" sClass="text-danger" large />
                        </a>
                      </PaddedColAbsolute>
                    )}
                  </RelativeRow>
                )
              })}
            </Col>
          </Form>
        ) : (
          <Table striped>
            <thead>
              <tr>
                <th>Firmware Version</th>
                <th>Serial Number</th>
              </tr>
            </thead>
            <tbody>
              {_.map(this.deviceDetails, (row, key) => {
                return (
                  <tr>
                    {_.map(row, (d, index) => {
                      return <td>{d}</td>
                    })}
                  </tr>
                )
              })}
            </tbody>
          </Table>
        )}
      </Modal>
    )
  }
}
