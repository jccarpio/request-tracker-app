/* @flow */

import { action, computed, observable } from 'mobx'
import { observer } from 'mobx-react'
import MergeTicketModal from './MergeTicketModal'
import React, { Component } from 'react'
import SplitTicketPopup from './SplitTicketPopup'
import styled from 'styled-components'
import TicketReply from './TicketReply'

const BottomFixedContainer = styled.div`
  background: #fff;
  bottom: 0;
  position: fixed;
  z-index: 99;
  width: ${props => (props.width ? props.width : '100%')};
  left: ${props => (props.isSplitPopupOpen ? '0' : 'auto')};
  right: ${props => (props.isSplitPopupOpen ? '0' : 'auto')};
`

@observer
export default class TicketConversationBottomWidgets extends Component {
  componentDidMount() {
    window.addEventListener('resize', this.calculateTicketContainerWidth)

    this.calculateTicketContainerWidth()
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.calculateTicketContainerWidth)
  }

  @action.bound
  calculateTicketContainerWidth() {
    const width = document.getElementById('ticket-container').clientWidth
    this.bottomFixedContainerWidth = `${width}px`
  }

  @observable isReplyOpen = false
  @observable isMergeModalOpen = false
  @observable isSplitPopupOpen = false
  @observable isSplitPopupFormOpen = false
  @observable bottomFixedContainerWidth = '100%'

  @computed
  get bottomFixedContainerHeight() {
    if (this.isReplyOpen) {
      return '450px'
    } else if (this.isSplitPopupFormOpen) {
      return '561px'
    } else if (this.isSplitPopupOpen) {
      return '86px'
    }
    return '60px'
  }

  @action.bound
  toggleMergeModal() {
    this.isMergeModalOpen = !this.isMergeModalOpen
  }

  @action.bound
  toggleReplyOpen() {
    this.isReplyOpen = !this.isReplyOpen
  }

  @action.bound
  toggleSplitPopUp() {
    this.isSplitPopupOpen = !this.isSplitPopupOpen
    if (this.isSplitPopupOpen) {
      this.isReplyOpen = false
    } else {
      this.isSplitPopupFormOpen = false
    }
  }

  @action.bound
  toggleSplitPopUpForm() {
    this.isSplitPopupFormOpen = !this.isSplitPopupFormOpen
  }

  @action.bound
  handleChange(editorVal: string) {
    return null
  }

  renderTicketReply() {
    return (
      <TicketReply
        onInputChange={this.handleChange}
        onToggleReplyOpen={this.toggleReplyOpen}
        isReplyOpen={this.isReplyOpen}
        onMergeClick={this.toggleMergeModal}
        onSplitClick={this.toggleSplitPopUp}
        ticketId={this.props.ticketId}
      />
    )
  }

  renderMergeTicketModal() {
    return <MergeTicketModal showModal={this.isMergeModalOpen} toggleModal={this.toggleMergeModal} />
  }

  render() {
    return (
      <BottomFixedContainer
        isSplitPopupOpen={this.isSplitPopupOpen}
        width={!this.isSplitPopupOpen && this.bottomFixedContainerWidth}
      >
        {!this.isSplitPopupOpen && this.renderTicketReply()}
        {this.isSplitPopupOpen && (
          <SplitTicketPopup toggleForm={this.toggleSplitPopUpForm} togglePopup={this.toggleSplitPopUp} />
        )}
        {this.isMergeModalOpen && this.renderMergeTicketModal()}
      </BottomFixedContainer>
    )
  }
}
