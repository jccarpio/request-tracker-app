/* @flow */
import { action, computed, observable, toJS } from 'mobx'
import { autobind } from 'core-decorators'
import { Button } from '../Styled'
import { Col, ControlLabel, Form, FormControl, FormGroup, HelpBlock, Row } from 'react-bootstrap'
import { getTicketPriorities, getTicketQueues, splitTicket } from './services'
import { inject, observer } from 'mobx-react'
import { required } from '../Helpers/Validations'
import { TICKET_PRIORITY, TICKET_STATUSES } from '../constants'
import _ from 'lodash'
import React, { Component } from 'react'
import SelectDropdown from '../common/SelectDropdown'
import SplitTicketFormModal from './SplitTicketFormModal'
import styled from 'styled-components'
import TicketRepository from './TicketRepository'
import WysiwygEditor from '../common/WysiwygEditor'

const StyledPopup = styled.div`
  width: 100%;
  background: #fff;
  border: 1px solid #ccc;
  z-index: 999;
  padding: 10px;
  display: ${props => (props.step !== 1 ? 'none' : 'block')};
`

const SplitTicketPopupACtions = styled.div`
  text-align: center;
  width: 100%;
  padding: 5px;
`
type Props = {
  ticketRepository: TicketRepository,
}

const DEFAULT_FIELD_DATA = {
  attachmentUniqueId: { value: '', hasChanged: false },
  inquiryDetail: { value: '', hasChanged: false, hasText: false },
  ticketQueueId: { value: '', hasChanged: false },
  priorityId: { value: '', hasChanged: false },
  subject: { value: '', hasChanged: false },
}

@inject('ticketRepository')
@observer
export default class SplitTicketPopup extends Component {
  static FIELDS: Object = {
    subject: { label: 'Ticket Subject', type: 'text', required: true },
    priorityId: { label: 'Priority', type: 'select', options: TICKET_STATUSES, required: true },
    ticketQueueId: { label: 'Queue', type: 'select', options: TICKET_PRIORITY, required: true },
    inquiryDetail: { label: 'Inquiry Details', type: 'wysiwyg', required: true },
  }

  props: Props

  @observable currStep = 1
  @observable ticketId = ''
  @observable ticketPriorities: Array<object> = []
  @observable ticketQueues: Array<object> = []
  richTextEditor: any
  @observable formValues = DEFAULT_FIELD_DATA
  @observable isUploadingAttachments: boolean = false
  @observable isSplitModalOpen: boolean = false
  @observable isSplittingTicket: boolean = false

  @computed
  get formErrors(): any {
    const { validateField } = this
    return {
      inquiryDetail: validateField('inquiryDetail'),
      ticketQueueId: validateField('ticketQueueId'),
      priorityId: validateField('priorityId'),
    }
  }

  @computed
  get isFormDisabled(): boolean {
    const errors = _.values(this.formErrors)
    return Boolean(_.find(errors, error => Boolean(error)))
  }

  @autobind
  validateField(fieldName: string): ?string {
    const { required: isRequired, type } = SplitTicketPopup.FIELDS[fieldName]
    const { value, hasText } = this.formValues[fieldName]
    let error = null

    if (isRequired && type !== 'wysiwyg') {
      error = required(value)
    } else if (isRequired && type === 'wysiwyg') {
      error = required(hasText)
    }

    return error
  }

  @action.bound
  nextStep() {
    this.currStep += 1
    this.props.toggleForm()
    if (this.currStep === 2) {
      this.toggleSplitModal()
    }
  }

  @action.bound
  prevStep() {
    this.currStep -= 1
    if (this.currStep === 1) {
      this.toggleSplitModal()
    }
  }

  @action.bound
  async handleSubmit() {
    this.isSplittingTicket = true
    const { splitTicketPayload, ticketDetails } = this.props.ticketRepository
    const { history, togglePopup } = this.props
    const { formValues } = this
    splitTicketPayload.ticket = {
      ...splitTicketPayload.ticket,
      ..._.mapValues(formValues, v => v.value),
    }

    const { data, errors } = await splitTicket(ticketDetails.ticketId, splitTicketPayload)
    if (!errors.length) {
      alert('Ticket was successfully splitted')
      this.richTextEditor.clearEditorState()
      this.props.ticketRepository.resetSplitTicketData()
      this.currStep = 1
      this.formValues = DEFAULT_FIELD_DATA
      togglePopup()
      this.toggleSplitModal()
      history.push(`/ticket-conversation/${data.ticketId}`)
    } else {
      this.apiErrors = errors
    }
    this.isSplittingTicket = false
  }

  @action.bound
  handleInputChange(e) {
    this.formValues[e.target.name].value = e.target.value
    this.formValues[e.target.name].hasChanged = true
  }

  @action.bound
  handleEditorChange(value: string, hasText: boolean) {
    this.formValues.inquiryDetail.value = value
    this.formValues.inquiryDetail.hasText = hasText
  }

  @action.bound
  handleEditorBlur(value: string, hasText: boolean) {
    this.formValues.inquiryDetail = {
      value,
      hasText,
      hasChanged: true,
    }
  }

  @action.bound
  onAttachmentUploadStart() {
    this.isUploadingAttachments = true
  }

  @action.bound
  onAttachmentUploadFailed() {
    this.isUploadingAttachments = false
  }

  @action.bound
  handleAttachmentChange(attachmentUniqueId: string, hasVirus: boolean) {
    this.isUploadingAttachments = false
    this.attachmentsHasVirus = hasVirus
    this.formValues.attachmentUniqueId = {
      value: attachmentUniqueId,
    }
  }

  @action.bound
  toggleSplitModal() {
    this.isSplitModalOpen = !this.isSplitModalOpen
  }

  @action.bound
  async onGetTicketInfoDropdownValues() {
    const [priorities, ticketQueues] = await Promise.all([getTicketPriorities(), getTicketQueues()])

    this.ticketPriorities = priorities.data.map(priority => {
      return { text: priority.description, value: priority.id }
    })

    this.ticketQueues = ticketQueues.data.map(ticketQueue => {
      return { text: ticketQueue.description, value: ticketQueue.id }
    })
  }

  componentWillMount() {
    this.onGetTicketInfoDropdownValues()
  }

  renderRequiredMarker() {
    return <span className="required">*</span>
  }

  renderFields(): * {
    return Object.keys(SplitTicketPopup.FIELDS).map((name, index) => {
      const { label, type = 'text', required, options = [] }: FormField = SplitTicketPopup.FIELDS[name]
      const { value, hasChanged } = this.formValues[name]
      const error = this.formErrors[name]
      const showError = Boolean(error) && hasChanged
      return (
        <FormGroup key={index} validationState={showError ? 'error' : null}>
          <Col lg={3} componentClass={ControlLabel} className="text-left">
            {label}
            {required && this.renderRequiredMarker()}
          </Col>
          <Col lg={8}>
            {type === 'wysiwyg' && (
              <WysiwygEditor
                onChange={this.handleEditorChange}
                onBlur={this.handleEditorBlur}
                textFieldHelpBlock={showError ? error : null}
                onAttachmentUploadStart={this.onAttachmentUploadStart}
                onAttachmentUploadComplete={this.handleAttachmentChange}
                onAttachmentUploadFailed={this.onAttachmentUploadFailed}
                ref={editor => {
                  this.richTextEditor = editor
                }}
              />
            )}
            {type === 'select' && (
              <span>
                <SelectDropdown
                  value={value}
                  options={name === 'ticketQueueId' ? this.ticketQueues : this.ticketPriorities}
                  name={name}
                  onChange={this.handleInputChange}
                />
                {showError && <HelpBlock>{error}</HelpBlock>}
              </span>
            )}
            {['text'].indexOf(type) > -1 &&
              name !== 'deviceDetails' && (
                <div>
                  <FormControl
                    className={name}
                    value={value}
                    name={name}
                    type={type}
                    onChange={this.handleInputChange}
                    onBlur={this.handleInputBlur}
                  />
                  {showError && <HelpBlock>{error}</HelpBlock>}
                </div>
              )}
          </Col>
        </FormGroup>
      )
    })
  }

  render() {
    const { togglePopup, ticketRepository } = this.props
    const { splitTicketPayload } = ticketRepository
    return (
      <StyledPopup step={this.currStep}>
        {this.currStep === 1 ? (
          <div className="text-center">
            Please Select from the conversation the message you want to export to the newly split Ticket
          </div>
        ) : null}
        <Row>
          <SplitTicketPopupACtions>
            <Button
              bsStyle="primary"
              onClick={this.currStep === 2 ? this.handleSubmit : this.nextStep}
              disabled={
                !splitTicketPayload.ticketMessageIds.length ||
                (this.isFormDisabled && this.currStep === 2) ||
                this.isUploadingAttachments
              }
            >
              {this.currStep === 2 ? 'Submit' : 'Next'}
            </Button>{' '}
            <Button onClick={togglePopup}>Cancel</Button>
          </SplitTicketPopupACtions>
        </Row>
        {this.currStep === 2 || this.currStep === 3 ? (
          <SplitTicketFormModal
            show={this.isSplitModalOpen}
            renderFields={this.renderFields.bind(this)}
            toggleModal={this.prevStep}
            currStep={this.currStep}
            prevStep={this.prevStep}
            nextStep={this.nextStep}
            handleSubmit={this.handleSubmit}
            isSplittingTicket={this.isSplittingTicket}
            hasError={this.formErrors}
            isUploadingAttachments={this.isUploadingAttachments}
          />
        ) : null}
      </StyledPopup>
    )
  }
}
