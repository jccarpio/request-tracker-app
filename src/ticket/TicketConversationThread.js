/* @flow */

import { action, observable } from 'mobx'
import { Button } from '../Styled/'
import { Col, Row } from 'react-bootstrap'
import { getFormattedDateTimeString } from '../Helpers/dateFormatters'
import { inject, observer } from 'mobx-react'
import _ from 'lodash'
import Avatar from '../common/Avatar'
import Icon from '../common/Icon'
import MessageContainer from '../common/MessageContainer'
import React, { Component } from 'react'
import styled from 'styled-components'

const StyleTicketConversationThread = styled(Row)`
  margin: 0;
  padding: 15px 0;
  .avatar-container {
    padding: 0;
  }
  .avatar {
    text-align: center;
  }
  .ticket-message-container {
    padding: 0 10px;
    box-sizing: border-box;
    width: calc(100% - 90px);
  }
  .conversation-item {
    margin: 20px;
  }
  .mid-content {
    margin-left: auto;
    margin-right: auto;
  }

  .show-prev-button {
    text-align: center;
  }

  .message-selector {
    width: 50px;
    text-align: center;
    display: block;
    vertical-align: middle;
    padding: 20px 10px;
  }
  .error-message {
    text-align: center;
    padding: 30px;
  }
  .error-item {
    margin: 20px 0;
  }
`

const StyledMessage = styled.div`
  .gmail_extra {
    display: none;
  }

  .email-message-container {
    .gmail_extra {
      display: ${props => (props.gmailMessageVisible ? 'block' : 'none')};
    }
    display: ${props => (props.gmailMessageVisible ? 'block' : 'none')};
    background-color: rgba(0, 0, 0, 0.1);
    padding: 10px;
    margin-top: 5px;
  }
  .button-container {
    i {
      padding: 5px 10px;
      background-color: rgba(0, 0, 0, 0.1);
      border-radius: 5px;
      font-size: 20px;
    }
  }
`

type ConversationItem = {
  leftContent?: any,
  midContent?: any,
  rightContent?: any,
  isRight?: any,
  key?: any,
  type?: string,
}

@inject('ticketRepository', 'userRepository')
@observer
export default class TicketConversationThread extends Component<*> {
  @observable gmailMessageVisible = false

  @action.bound
  onShowGmailMessage() {
    this.gmailMessageVisible = !this.gmailMessageVisible
  }
  componentDidMount() {
    this.props.ticketRepository.onGetTicketMessages(this.props.ticketId)
  }

  componentWillReceiveProps(nextProps: any) {
    if (this.props.ticketId !== nextProps.ticketId) {
      this.props.ticketRepository.onGetTicketMessages(nextProps.ticketId)
    }
    if (this.props.userId !== nextProps.userId) {
      this.props.ticketRepository.onGetTicketMessages(this.props.ticketId)
    }
  }

  renderConversationItem({
    key,
    leftContent,
    midContent,
    rightContent,
    isRight = false,
    type,
    user,
  }: ConversationItem) {
    const pullClass = isRight ? 'pull-right' : 'pull-left'
    const { isSplitPopupOpen, ticketRepository } = this.props
    const { toggleSplitTicketMessageIds } = ticketRepository
    const isMessage = type === 'message'
    return (
      <Row className="conversation-item" key={key}>
        <Col md={1} className="message-selector pull-left">
          {isMessage &&
            isSplitPopupOpen && (
              <input
                type="checkbox"
                onChange={e => {
                  toggleSplitTicketMessageIds(e.target.checked, key, user)
                }}
              />
            )}
        </Col>

        <Col md={1} className={`${pullClass} avatar-container`}>
          {leftContent}
        </Col>
        <Col md={9} className={`mid-content`}>
          {midContent}
        </Col>
        <Col md={1} className={`${pullClass} avatar-container`}>
          {rightContent}
        </Col>
      </Row>
    )
  }

  renderLoading() {
    return (
      <div className="text-center">
        <Icon iconName="spinner fa-spin" xlarge />
      </div>
    )
  }

  renderHtmlMessage(content: string) {
    const isForwardedGmail = content.includes('gmail_extra')
    if (!isForwardedGmail) {
      return <div dangerouslySetInnerHTML={{ __html: content }} />
    }

    const htmlContainer = document.createElement('DIV')
    htmlContainer.innerHTML = content

    const gmailElement = htmlContainer.getElementsByClassName('gmail_extra')

    return (
      <StyledMessage gmailMessageVisible={this.gmailMessageVisible}>
        <div dangerouslySetInnerHTML={{ __html: htmlContainer.outerHTML }} />
        <div onClick={this.onShowGmailMessage} className="button-container">
          <Icon iconName={this.gmailMessageVisible ? 'chevron-up' : 'ellipsis-h'} />
        </div>
        <div className="email-message-container">
          <div dangerouslySetInnerHTML={{ __html: gmailElement[0].outerHTML }} />
        </div>
      </StyledMessage>
    )
  }

  renderTicketMessages(): any {
    const {
      sortedTicketMessages = {},
      getTicketMessageErrors = [],
      onDeleteMessagePressed,
      isGettingTicketMessage,
    } = this.props.ticketRepository

    if (!_.isEmpty(getTicketMessageErrors)) {
      return (
        <div className="error-message">
          {getTicketMessageErrors.map(({ message }, index) => (
            <div className="error-item" key={index}>
              {message}
            </div>
          ))}
          {!isGettingTicketMessage && (
            <Button
              onClick={() => this.props.ticketRepository.onGetTicketMessages(this.props.ticketId)}
              bsStyle="primary"
            >
              Try Again
            </Button>
          )}
        </div>
      )
    }

    return _.map(
      sortedTicketMessages,
      ({ message, creationDate, lastUpdateDate, createdBy, id, isLoading, attachments, userInfoDisplay }) => {
        const messageFromViewer = userInfoDisplay && userInfoDisplay.id === this.props.userRepository.currentUser.id
        const ownerRole = messageFromViewer ? 'ROLE_VIEWER' : userInfoDisplay.role

        return this.renderConversationItem({
          key: id,
          type: 'message',
          leftContent: !messageFromViewer && <Avatar />,
          midContent: (
            <MessageContainer
              author={userInfoDisplay.fullName}
              ownerRole={ownerRole}
              repliedTimeStamp={`Replied at ${getFormattedDateTimeString(lastUpdateDate)}`}
              creator={createdBy}
              createdTimeStamp={getFormattedDateTimeString(creationDate)}
              isRight={messageFromViewer}
              onDeletePressed={() => onDeleteMessagePressed(id, this.props.ticketId)}
              // onReplyPressed={() => this.props.onReplyToMessageItem(message)}
              attachments={attachments}
              isLoading={isLoading}
            >
              {message ? this.renderHtmlMessage(message) : ''}
            </MessageContainer>
          ),
          rightContent: messageFromViewer && <Avatar />,
        })
      }
    )
  }

  render() {
    const { isLastPage, onShowPreviousMessage, isGettingTicketMessage, remainingMessages } = this.props.ticketRepository
    return (
      <StyleTicketConversationThread>
        {/* <TicketStatus>Ticket Split from #300114 by Susan Boylle (Mon 31-012018 18:34:00)</TicketStatus> */}
        {!isLastPage && (
          <div className="text-center">
            <Button
              bsStyle="primary"
              onClick={() => onShowPreviousMessage(this.props.ticketId)}
            >{`Show previous message (${remainingMessages})`}</Button>
          </div>
        )}
        {isGettingTicketMessage && this.renderLoading()}
        {this.renderTicketMessages()}
      </StyleTicketConversationThread>
    )
  }
}
