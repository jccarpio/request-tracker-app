/* @flow */

import { Panel } from '../Styled/'
import React, { Component } from 'react'

export default class TicketStatus extends Component {
  render() {
    const { children } = this.props
    return <Panel>{children}</Panel>
  }
}
