/* @flow */

import { action, computed, observable } from 'mobx'
import { AddDeviceAction, PaddedCol, PaddedColAbsolute, RelativeRow } from '../styled'
import { autobind } from 'core-decorators'
import { Button, Col, ControlLabel, Form, FormControl, FormGroup, Row } from 'react-bootstrap'
import { editTicketDetails } from '../services/'
import { Icon } from '../../Styled/'
import { inject, observer } from 'mobx-react'
import { injectGlobal } from 'styled-components'
import { mergeTicket } from '../services/'
import { notify } from 'react-notify-toast'
import { required } from '../../Helpers/Validations'
import _ from 'lodash'
import Countries from 'country-list'
import ErrorsList from '../../common/ErrorsList'
import Modal from '../../common/Modal'
import React, { Component } from 'react'
import SearchableSelectDropdown from '../../common/SearchableSelectDropdown'
import TicketRepository from '../TicketRepository'

const countryList = Countries()
  .getNames()
  .map(country => ({ value: country, label: country }))

type Props = {
  ticketRepository: TicketRepository,
  toggleModal: () => void,
}

@inject('ticketRepository')
@observer
export default class BasicInfo extends Component {
  props: Props

  @observable notify = notify.createShowQueue()
  @observable isUpdating: boolean = false
  @observable apiErrors: ?Array<ApiError> = null

  static FIELDS: Object = {
    inquiryTypeId: { label: 'Type of Inquiry', required: true, type: 'select' },
    country: { label: 'Country', type: 'select' },
    pointOfPurchase: { label: 'Point of Purchase' },
  }

  @observable
  formValues = {
    inquiryTypeId: { value: '', hasChanged: false },
    country: { value: '', hasChanged: false },
    pointOfPurchase: { value: '', hasChanged: false },
  }

  @computed
  get formErrors(): any {
    const { validateField } = this
    return {
      inquiryTypeId: validateField('inquiryTypeId'),
    }
  }

  @autobind
  validateField(fieldName: string): ?string {
    const { required: isRequired, type } = BasicInfo.FIELDS[fieldName]
    const { value, hasText } = this.formValues[fieldName]
    let error = null

    if (isRequired) {
      error = required(value)
    }

    return error
  }

  @computed
  get isFormDisabled(): boolean {
    const errors = _.values(this.formErrors)
    return this.isUpdating || Boolean(_.find(errors, error => Boolean(error)))
  }

  @action.bound
  async handleSubmit(e: any) {
    this.isUpdating = true
    let { ticketId, deviceDetails } = this.props.ticketRepository.ticketDetails
    const { data, errors } = await editTicketDetails(ticketId, _.mapValues(this.formValues, v => v.value))
    if (!errors.length) {
      this.props.ticketRepository.ticketDetails = data
      this.notify(`Basic Information is updated.`, 'success')
      this.props.toggleModal()
    } else {
      this.apiErrors = errors
      this.notify(`Failed to update Basic Information, Error: ${errors[0].message}`, 'error')
    }
    this.isUpdating = false
  }

  @action.bound
  handleInputChange(e: *) {
    const { name, value } = e.target
    this.formValues[name].value = value
  }

  @action.bound
  handleCountryChange(selectedOption) {
    this.formValues.country.value = selectedOption ? selectedOption.value : ''
  }

  @action.bound
  handleInquiryTypeChange(selectedOption) {
    this.formValues.inquiryTypeId.value = selectedOption ? selectedOption.value : ''
  }

  renderRequiredMarker() {
    return <span className="required">*</span>
  }

  componentWillMount() {}

  componentDidMount() {
    const { ticketDetails } = this.props.ticketRepository
    const { inquiryTypeId, pointOfPurchase, country } = ticketDetails
    this.formValues.inquiryTypeId.value = inquiryTypeId
    this.formValues.pointOfPurchase.value = pointOfPurchase
    this.formValues.country.value = country
  }

  renderFields(): * {
    const { inquiryTypes } = this.props.ticketRepository
    return Object.keys(BasicInfo.FIELDS).map((name, index) => {
      const { label, type = 'text', required = false }: FormField = BasicInfo.FIELDS[name]
      const { value, hasChanged } = this.formValues[name]
      const error = this.formErrors[name]
      const showError = Boolean(error) && hasChanged
      return (
        <FormGroup key={index} validationState={showError ? 'error' : null}>
          <Col lg={4} componentClass={ControlLabel} className="text-left">
            {label}
            {required && this.renderRequiredMarker()}
          </Col>
          <Col lg={8}>
            {type === 'select' && (
              <SearchableSelectDropdown
                options={name === 'country' ? countryList : inquiryTypes}
                onChange={name === 'country' ? this.handleCountryChange : this.handleInquiryTypeChange}
                placeholder="Select a country"
                value={value}
              />
            )}
            {['text'].indexOf(type) > -1 &&
              name !== 'deviceDetails' && (
                <div>
                  <FormControl
                    className={name}
                    value={value}
                    name={name}
                    type={type}
                    onChange={this.handleInputChange}
                    onBlur={this.handleInputBlur}
                  />
                  {showError && <HelpBlock>{error}</HelpBlock>}
                </div>
              )}
          </Col>
        </FormGroup>
      )
    })
  }

  render() {
    const { toggleModal } = this.props
    return (
      <Modal
        header={'Edit Basic Info'}
        footer={
          <Row>
            <Col lg={12} className="text-center">
              <Button bsSize="small" bsStyle="primary" onClick={this.handleSubmit} disabled={this.isFormDisabled}>
                {`${this.isUpdating ? 'Saving...' : 'Save'}`}
              </Button>{' '}
              <Button bsSize="small" onClick={toggleModal} disabled={this.isUpdating}>
                Close
              </Button>
            </Col>
          </Row>
        }
        showModal
        onHide={toggleModal}
      >
        <Form horizontal onSubmit={this.handleSubmit}>
          <Col lg={12}>
            <p>{this.apiErrors && <ErrorsList errors={this.apiErrors} />}</p>
          </Col>
          <Col lg={12}>{this.renderFields()}</Col>
        </Form>
      </Modal>
    )
  }
}
