/* @flow */

// notes for api are in comments
// added fields are not yet in API docs
// incorrect types in docs compared with actual return

export type TicketDetails = {
  closedDate: ?string,
  creationDate: ?string, // unix date as of now
  firmwareVersion: ?string,
  splitTicketId: ?number,
  inquiryDetail: ?string,
  startedDate: ?string,
  lastUpdateDate: ?string, // unix date as of now
  owner: Object,
  otherOwner: Object,
  pointOfPurchase: string,
  requestor: Object, // missing from api, currently has requestorId
  ticketId: ?string, // is it string or int
  mergeTicketId: ?number,
  tags: Array<string>,
  forumURL: ?string,
  country: ?string,
  attachments: Array<any>, // will replace any once attachment type has been defined
  email: string,
  previousStatus: ?string,
  dueDate: ?string,
  subject: string,
  serialNumber: string,
  attachmentFiles: Array<any>, // will replace any once attachment type has been defined
  ticketQueue: ?Object,
  inquiryTypeId: ?number,
  lastContactDate: string,
  inquiryType: {
    code: string,
    description: string,
    id: number,
    version: number,
    creationDate: string,
  },
  ticketQueueId: ?number,
  status: string,
  name: string,
  priority: ?string,
  lastUpdateBy: any,
  createdBy: any,
  id: number,
  version: number,
}

export type TicketAttachment = {
  resourcePath: string,
  fileSize: string,
  success: boolean,
  remark: string,
  fileName: string,
  path: string,
  creationDate: string,
  owner: Object,
  resourcePathThumbnail?: string,
}
