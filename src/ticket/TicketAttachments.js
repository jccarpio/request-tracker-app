/* @flow */

import { Button } from '../Styled'
import { observer } from 'mobx-react'
import { type TicketAttachment } from './types'
import Attachment from './Attachment'
import Icon from '../common/Icon'
import React, { Component } from 'react'

type Props = {
  ticketId: number,
  isLoadingAttachments: boolean,
  loadingAttachmentsFailed: boolean,
  attachments: Array<TicketAttachment>,
  onGetTicketAttachments: (ticketId: number) => Promise<void>,
}

@observer
export default class TicketAttachments extends Component {
  componentDidMount() {
    const { ticketId, onGetTicketAttachments } = this.props
    onGetTicketAttachments(ticketId)
  }

  renderLoading() {
    return (
      <div className="text-center">
        <Icon xlarge iconName="spinner fa-spin" />
      </div>
    )
  }

  renderAttachmentsListOrError() {
    const { ticketId, onGetTicketAttachments, attachments } = this.props
    if (this.loadingAttachmentsFailed) {
      return (
        <div className="text-center">
          Failed to load attachments
          <div>
            <Button onClick={() => onGetTicketAttachments(ticketId)} bsStyle="primary">
              Try Again
            </Button>
          </div>
        </div>
      )
    }
    if (!attachments.length) {
      return <div className="text-center">This ticket has no attachments</div>
    }
    return attachments.map((attachment: TicketAttachment, index: number) => {
      return <Attachment className="sidebar-" key={index} data={attachment} />
    })
  }

  props: Props
  render() {
    return <div>{this.props.isLoadingAttachments ? this.renderLoading() : this.renderAttachmentsListOrError()}</div>
  }
}
