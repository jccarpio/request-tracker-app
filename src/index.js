/* @flow */

import './app.css'
import './Styled/'
import 'font-awesome/css/font-awesome.min.css'
import { Provider } from 'mobx-react'
import Main from './main/Main'
import mobxStore from './mobxStore'
import React from 'react'
import ReactDOM from 'react-dom'

if (document.getElementById('request-tracker-app')) {
  ReactDOM.render(
    <Provider {...mobxStore}>
      <Main />
    </Provider>,
    document.getElementById('request-tracker-app')
  )
}
