/* @flow */

import _ from 'lodash'
import axios, { CancelToken } from 'axios'

// const CancelToken = axios.CancelToken
// const sources = CancelToken.source()

// const cancelToken = {
//   cancelToken: sources.token,
// }

// const CANCELLED_REQUEST = 'Network Request Cancelled'
// export const cancelRequest = () => {
//   sources.cancel(CANCELLED_REQUEST)
// }

export type ApiError = {
  message: string,
  fieldName?: string,
}

export type ApiResponse = {
  data: Object,
  errors?: Array<ApiError>,
}

export const forcedLogout = () => {
  window.location.href = '/login'
  window.location.reload()
}

let cancelRequestInstance = (errorMessage: string) => null

const CANCELLED_REQUEST = `message request cancelled`

export const cancelRequest = () => {
  cancelRequestInstance(CANCELLED_REQUEST)
}

export const apiHasErrors = ({ data, errors }: { data: any, errors: Array<ApiError> }) => !_.isEmpty(errors)

const getAPIinstance = () =>
  axios.create({
    baseURL: '/',
    headers: { 'Content-Type': 'application/json;charset=utf-8' },
    cancelToken: new CancelToken(cancel => {
      cancelRequestInstance = cancel
    }),
  })

const responseParser = response => {
  const { data } = response
  return {
    data: data.errorMessagesList ? {} : data,
    errors: data.errorMessagesList || [],
  }
}

const errorParser = e => {
  if (e.message && e.message === CANCELLED_REQUEST) {
    return { data: {} }
  }

  const { data } = e.response
  return {
    data: {},
    errors: data.errorMessagesList || [{ message: 'Your request cannot be completed as of now' }],
  }
}

let setHeaders = () => {
  if (_.has(localStorage, 'TICKET_SESSION')) {
    // const TICKET_SESSION = JSON.parse(localStorage.getItem('TICKET_SESSION'))
    // APIinstance.defaults.headers['Authorization'] = TICKET_SESSION.token
  }
}

export const get = async (url: string, payload: any = {}) => {
  setHeaders()

  return getAPIinstance()
    .get(`${url}`, payload)
    .then(responseParser)
    .catch(errorParser)
}

export async function post(url: string, payload: any = {}) {
  setHeaders()
  return getAPIinstance()
    .post(`${url}`, payload)
    .then(responseParser)
    .catch(errorParser)
}

export async function update(url: string, payload: any = {}) {
  setHeaders()
  return getAPIinstance()
    .put(`${url}`, payload)
    .then(responseParser)
    .catch(errorParser)
}

export async function patch(url: string, payload: any = {}) {
  setHeaders()
  return getAPIinstance()
    .patch(`${url}`, payload)
    .then(responseParser)
    .catch(errorParser)
}

export async function del(url: string, payload: any = {}) {
  setHeaders()
  return getAPIinstance()
    .delete(`${url}`, payload)
    .then(responseParser)
    .catch(errorParser)
}
