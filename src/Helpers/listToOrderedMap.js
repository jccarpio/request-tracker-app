/* @flow */

import _ from 'lodash'

export default (list: Array<any>): Object =>
  _.reduce(
    list,
    (newObject, item) => {
      return _.set(newObject, [item.id], item)
    },
    {}
  )
