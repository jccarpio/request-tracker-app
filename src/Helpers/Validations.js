/* @flow */

export const required = (value: string): ?string => {
  return value ? null : 'This field is required'
}

export const minLength = (min: number) => (value: string): ?string => {
  return value && value.length < min ? `Must be ${min} characters or more` : null
}

export const maxLength = (max: number) => (value: string): ?string => {
  return value && value.length > max ? `Upto ${max} characters only` : null
}

export const equalLength = (equalTo: number) => (value: string): ?string => {
  return value && value.length !== equalTo ? `Must be ${equalTo} characters` : null
}

export const number = (value: *): ?string => {
  return value && isNaN(Number(value)) ? 'Must be a number' : null
}

export const email = (value: string): ?string => {
  return value && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value) ? 'Invalid email address' : null
}
