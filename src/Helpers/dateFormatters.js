/* @flow */

import moment from 'moment'

export const getFormattedDateString = (dateString: any, defaultValue: string = 'N/A') => {
  const isValidDate = moment(dateString).isValid()

  if (!isValidDate) {
    return defaultValue
  }

  return moment(dateString).format('DD MMM YYYY')
}

export const getFormattedDateTimeString = (dateTimeString: any, defaultValue: string = 'N/A') => {
  const isValidDate = moment(dateTimeString).isValid()

  if (!isValidDate) {
    return defaultValue
  }

  return moment(dateTimeString).format('DD MMM YYYY HH:mm')
}
