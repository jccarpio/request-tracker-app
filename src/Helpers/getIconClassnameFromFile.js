/* @flow */
import _ from 'lodash'

const getIconClassnameFromFile = (file: File): string => {
  const fileType = _.last(file.name.split('.'))
  switch (fileType) {
    case 'png':
    case 'jpg':
    case 'bmp':
    case 'gif':
      return 'file-image-o'

    case 'mp3':
    case 'wav':
    case 'mpeg-4':
      return 'file-audio-o'

    case 'avi':
    case 'flv':
    case 'mwv':
    case 'mp4':
      return 'file-movie-o'

    case 'zip':
    case 'rar':
    case 'tar.gz':
    case 'gzip':
    case 'iso':
      return 'file-archive-o'

    case 'php':
    case 'js':
    case 'css':
    case 'html':
    case 'htm':
    case 'java':
    case 'py':
    case 'scss':
    case 'json':
    case 'sql':
      return 'file-code-o'

    case 'doc':
    case 'docx':
      return 'file-word-o'

    case 'xls':
    case 'xlsx':
      return 'file-excel-o'

    case 'pdf':
      return 'file-pdf-o'

    case 'txt':
    case 'plain':
      return 'file-text-o'

    default:
      return 'file-o'
  }
}

export default getIconClassnameFromFile
