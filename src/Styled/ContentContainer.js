import styled from 'styled-components'

const getPaddingValue = props => {
  let padding = 10
  if (props.paddingSmall) {
    padding = 5
  }
  if (props.paddingLarge) {
    padding = 15
  }
  return `${padding}px`
}

const ContentContainer = styled.section`
  padding: ${props => getPaddingValue(props)};
`
export default ContentContainer
