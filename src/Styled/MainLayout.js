/* @flow */

import { COLOR_SECONDARY } from './themeColours'
import styled from 'styled-components'

const SIDEBAR_WIDTH = 340

export default styled.section`
  max-width: 1366px;
  height: 100%;
  margin: 0 auto;
  padding: 0 10px;
  position: relative;
  min-height: calc(100vh - 100px);
  .sidebar {
    color: ${COLOR_SECONDARY};
    width: ${SIDEBAR_WIDTH}px;
    z-index: 2;
    background: rgba(255, 255, 255, 1);
  }
  .main-container {
    display: table;
    width: 100%;
    color: rgba(51, 51, 51, 1);
    position: relative;
    box-sizing: border-box;
    min-height: calc(100vh - 100px);
  }
  .sidebar,
  .main-content {
    display: table-cell;
    vertical-align: top;
  }
  .main-content {
    box-shadow: 3px 0px 3px -2px #666;
    background: rgba(255, 255, 255, 1);
    height: 100vh;
  }
`
