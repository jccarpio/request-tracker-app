/* @flow */

import { PANEL_ERROR, PANEL_PRIMARY } from './themeColours'
import styled from 'styled-components'

const getBackgroundColor = props => {
  const { panelColor } = props || PANEL_PRIMARY
  let color = PANEL_PRIMARY
  if (panelColor === 'error') {
    color = PANEL_ERROR
  }
  return color
}

export default styled.div`
  padding: 10px;
  background-color: ${props => getBackgroundColor(props)};
  box-shadow: 2px 2px 3px rgba(0, 0, 0, 0.2);
  text-align: center;
`
