import { NavLink } from 'react-router-dom'
import styled from 'styled-components'

export default styled(NavLink)`
  color: #2199e8;
  text-decoration: none;
`
