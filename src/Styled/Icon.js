import React, { PureComponent } from 'react'
import styled from 'styled-components'

const StyledIcon = styled.i`
  font-size: ${props => getFontSizeStyle(props)};
`
const getFontSizeStyle = props => {
  let fontSize = 14
  if (props.small) {
    fontSize = 10
  }
  if (props.large) {
    fontSize = 18
  }
  return `${fontSize}px`
}

export default class Icon extends PureComponent {
  render() {
    const { iconName, sClass } = this.props
    return (
      <StyledIcon style={this.sizeStyle} className={`fa fa-${iconName} ${sClass}`} aria-hidden="true" {...this.props} />
    )
  }
}
