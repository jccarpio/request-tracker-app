import { injectGlobal } from 'styled-components'
import Button from './Button'
import ContentContainer from './ContentContainer'
import Icon from './Icon'
import Image from './Image'
import Link from './Link'
import MainLayout from './MainLayout'
import Panel from './Panel'

injectGlobal`
  @import url("https://fonts.googleapis.com/css?family=Roboto:Thin,Regular,Medium,Ligth,Bold,100,300,400,500,600,700,800");
  @font-face {
    font-family: 'Roboto';
  }
  html {
    height: 100%;
  }
  body {
    background-color: #F2F2F2;
    ${'' /* font-family: 'Roboto'; */}
    font-style: normal;
    font-weight: 400;
    font-size:14px;
    min-height: 100%;
    a {
      text-decoration:none !important;
    }
  }
  .required {
    color: red;
  }

  .toast-notification {
    width:100% !important;
    top:100px;
    span {
      width:100% !important;
    }
  }
`
export { Button, Panel, Image, ContentContainer, Icon, MainLayout, Link }
