export const COLOR_PRIMARY = 'rgba(25,158,216,1)'
export const COLOR_SECONDARY = 'rgba(51, 51, 51, 1)'
export const COLOR_TERTIARY = 'rgba(255, 164, 0, 1)'
export const COLOR_DEFAULT = 'rgba(153, 153, 153, 1)'
export const COLOR_INVERSE = '#282828'

export const COLOR_ERROR = '#a94442'

export const PANEL_PRIMARY = 'rgba(198, 237, 255, 1)'
export const PANEL_ERROR = '#f2dede'
