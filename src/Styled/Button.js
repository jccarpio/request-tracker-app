import { Button } from 'react-bootstrap'
import { COLOR_DEFAULT, COLOR_PRIMARY, COLOR_SECONDARY, COLOR_TERTIARY } from '../Styled/themeColours'
import styled, { css } from 'styled-components'

const getBackgroundColorStyle = props => {
  let backgroundColor = COLOR_DEFAULT
  const color = 'rgba(255,255,255,1)'
  const { bsStyle } = props
  if (bsStyle === 'primary') {
    backgroundColor = COLOR_PRIMARY
  }
  if (bsStyle === 'secondary') {
    backgroundColor = COLOR_TERTIARY
  }
  return css`
     {
      background-color: ${backgroundColor};
      color: ${color};
      &: hover {
        background-color: ${backgroundColor};
        color: ${color};
      }
    }
  `
}

export default styled(Button)`
  border: none;
  margin-top: ${props => props.mtop && `${props.mtop}`}px;
  margin-right: ${props => props.mright && `${props.mright}`}px;
  border-radius: 5px;
  ${props => getBackgroundColorStyle(props)};
`
