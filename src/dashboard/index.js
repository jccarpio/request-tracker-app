/* @flow */

import { Panel as _Panel, Col, Row, Table } from 'react-bootstrap'
import { observer } from 'mobx-react'
import Avatar from '../common/Avatar'
import MainLayout from '../Styled/MainLayout'
import React, { Component } from 'react'
import ReactTable from '../common/ReactTable'
import styled from 'styled-components'
import TimeAgo from '../common/TimeAgo'

const PageHeader = styled(Col)`
  margin-top: 20px;
  .panel-body {
    height: 100px;
    .avatar {
      display: inline-block;
      height: 80px;
      vertical-align: middle;
      img {
        vertical-align: middle;
      }
    }
    h2 {
      margin-left: 20px;
      display: inline-block;
    }
  }
`
const Panel = styled(_Panel)`
  border-radius: 15px;
`
const PanelHeader = styled(_Panel.Heading)`
  background-color: #fff !important;
  border-bottom: none;
  border-radius: 15px;
  h4 {
    font-weight: 300;
    font-size: 20px;
    padding: 10px 15px;
  }
  a {
    margin-top: -40px;
    font-size: 12px;
  }
`
const PanelBody = styled(_Panel.Body)`
  padding: 15px 30px;
`

export default class Dashboard extends Component {
  static ticketsOwnedColumDef = [
    {
      Header: 'Number',
      accessor: 'ticketId',
    },
    {
      Header: 'Title',
      accessor: 'subject',
    },
    {
      Header: 'Queue',
      accessor: 'ticketQueue.description',
    },
    {
      Header: 'Status',
      accessor: 'status',
    },
    {
      Header: 'Created',
      accessor: 'creationDate',
      Cell: d => <TimeAgo date={new Date(d.row.creationDate)} />,
    },
    {
      Header: 'Owner',
      accessor: 'email',
    },
    {
      Header: 'SLA',
      accessor: 'sla',
    },
  ]

  static ticketsUnOwnedColumnDef = [
    {
      Header: 'Number',
      accessor: 'ticketId',
    },
    {
      Header: 'Title',
      accessor: 'subject',
    },
    {
      Header: 'Queue',
      accessor: 'ticketQueue.description',
    },
    {
      Header: 'Created',
      accessor: 'creationDate',
      Cell: d => <TimeAgo date={new Date(d.row.creationDate)} />,
    },
  ]

  render() {
    return (
      <MainLayout>
        <Row>
          <PageHeader lg={12}>
            <Panel>
              <PanelBody>
                <Avatar />
                <h2>Hello, John Doe</h2>
              </PanelBody>
            </Panel>
          </PageHeader>
        </Row>
        <Row>
          <Col lg={12}>
            <Panel>
              <PanelHeader>
                <h4>Tickets you own</h4>
                <a href="#" className="pull-right">
                  View more
                </a>
              </PanelHeader>
              <PanelBody>
                <ReactTable
                  columnDefs={Dashboard.ticketsOwnedColumDef}
                  defaultPageSize={10}
                  showPagination={false}
                  isServerSide={true}
                  endpoint="tickets"
                />
              </PanelBody>
            </Panel>
          </Col>
        </Row>
        <Row>
          <Col lg={4}>
            <Panel>
              <PanelHeader>
                <h4>Ticket Summary</h4>
                <a href="#" className="pull-right">
                  View more
                </a>
              </PanelHeader>
              <PanelBody>
                <Table>
                  <tbody>
                    <tr>
                      <td>Unresolved</td>
                      <td>999</td>
                    </tr>
                    <tr>
                      <td>Overdue</td>
                      <td>999</td>
                    </tr>
                    <tr>
                      <td>Due Today</td>
                      <td>999</td>
                    </tr>
                    <tr>
                      <td>Waiting for your feedback</td>
                      <td>999</td>
                    </tr>
                    <tr>
                      <td>Waiting for customer feedback</td>
                      <td>999</td>
                    </tr>
                    <tr>
                      <td>Unassigned</td>
                      <td>999</td>
                    </tr>
                  </tbody>
                </Table>
              </PanelBody>
            </Panel>
          </Col>
          <Col lg={8}>
            <Panel>
              <PanelHeader>
                <h4>Latest unowned tickets</h4>
                <a href="#" className="pull-right">
                  View more
                </a>
              </PanelHeader>
              <PanelBody>
                <ReactTable
                  columnDefs={Dashboard.ticketsUnOwnedColumnDef}
                  defaultPageSize={7}
                  showPagination={false}
                  isServerSide={true}
                  endpoint="tickets"
                />
              </PanelBody>
            </Panel>
          </Col>
        </Row>
      </MainLayout>
    )
  }
}
