import { get } from '../Helpers/API'

export const fetchTickets = () => {
  return get('/api/tickets')
}
