/* @flow */
import { Modal as _Modal } from 'react-bootstrap'
import { action, observable } from 'mobx'
import { observer } from 'mobx-react'
import React, { Component } from 'react'
import styled from 'styled-components'

const Modal = styled(_Modal)`
  .modal-content {
    border-radius: 0px;
  }
`
const ModalHeader = styled(_Modal.Header)`
  border: none;
`
const ModalTitle = styled(_Modal.Title)``
const ModalBody = styled(_Modal.Body)`
  input {
    border-radius: 0px;
  }
`
const ModalFooter = styled(_Modal.Footer)`
  border: none;
  text-align: center;
  button {
    padding: 10px;
    min-width: 100px;
  }
`

@observer
export default class ModalWindow extends Component {
  @observable showModal = this.props.showModal || false
  @observable showCloseBtn = this.props.showCloseBtn || false

  @action.bound
  handleClose() {
    this.showModal = false
  }

  @action.bound
  handleShow() {
    this.showModal = true
  }

  render() {
    const { header, footer, bsSize, className = '' } = this.props
    return (
      <Modal
        show={this.showModal}
        onHide={this.props.onHide}
        backdrop="static"
        bsSize={bsSize || ''}
        className={`${className}`}
      >
        {header && (
          <ModalHeader closeButton={this.showCloseBtn}>
            <ModalTitle>{this.props.header}</ModalTitle>
          </ModalHeader>
        )}
        <ModalBody>{this.props.children}</ModalBody>
        {footer && <ModalFooter>{this.props.footer}</ModalFooter>}
      </Modal>
    )
  }
}
