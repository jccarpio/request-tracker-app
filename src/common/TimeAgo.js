import React, { PureComponent } from 'react'
import TA from 'react-timeago'

const formatter = (value, unit, suffix, date) => {
  return `${value} ${unit}${value > 1 ? 's' : ''} ago`
}

export default class TimeAgo extends PureComponent {
  render() {
    const { date = new Date() } = this.props
    return <TA date={date} formatter={formatter} />
  }
}
