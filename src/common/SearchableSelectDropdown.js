/* @flow */

import 'react-select/dist/react-select.css'

import React, { PureComponent } from 'react'
import Select from 'react-select'

type Props = {
  onChange: (value: Object | string) => void,
  options: Array<{ value: string, text: string }> | string,
  value: string,
}

export default class SelectDropdown extends PureComponent<Props> {
  render() {
    const { options, onChange, value } = this.props
    return <Select name="form-field-name" searchable value={value} onChange={onChange} options={options} />
  }
}
