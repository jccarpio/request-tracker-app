/* @flow */

import React, { PureComponent } from 'react'
import styled from 'styled-components'

const StyledAvatarContainer = styled.div`
  img {
    height: ${props => props.size || 60}px;
    width: ${props => props.size || 60}px;
    border-radius: ${props => (props.size ? props.size / 2 : 60)}px;
    box-shadow: 1px 2px 4px rgba(0, 0, 0, 0.2), 1px 2px 3px rgba(0, 0, 0, 0.44);
  }
`

type Props = {
  imageUrl?: string,
  onClick?: () => void,
}

export default class Avatar extends PureComponent<Props> {
  render() {
    const { imageUrl = 'http://via.placeholder.com/151x94', onClick } = this.props
    return (
      <StyledAvatarContainer onClick={onClick} className="avatar">
        <img src={imageUrl} />
      </StyledAvatarContainer>
    )
  }
}
