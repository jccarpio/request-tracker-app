import React, { PureComponent } from 'react'
import styled from 'styled-components'

const StyledIcon = styled.i`
  font-size: ${props => getFontSizeStyle(props)};
`
const getFontSizeStyle = props => {
  let fontSize = 14
  if (props.small) {
    fontSize = 10
  }
  if (props.large) {
    fontSize = 18
  }

  if (props.xlarge) {
    fontSize = 24
  }
  return `${fontSize}px`
}

export default class CollapsibleWidget extends PureComponent {
  render() {
    const { iconName, onClick = () => null } = this.props
    return <StyledIcon style={this.sizeStyle} className={`fa fa-${iconName}`} aria-hidden="true" onClick={onClick} />
  }
}
