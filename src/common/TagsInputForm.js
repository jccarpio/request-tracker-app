/* @flow */

import { action, observable } from 'mobx'
import { autobind } from 'core-decorators'
import { observer } from 'mobx-react'
import Icon from '../common/Icon'
import React, { Component, type Node } from 'react'
import styled from 'styled-components'
import TagsInput from 'react-tagsinput'

const StyledTagsInput = styled.div`
  .tag-item {
    padding: 5px;
    border-radius: 5px;
    background-color: rgba(153, 153, 153, 1);
    margin-bottom: 10px;
    color: rgba(255, 255, 255, 1);
    margin-right: 5px;
  }
  .input-component {
    margin-bottom: 10px;
    input {
      width: 100%;
      font-size: 14px;
      padding: 5px;
      height: 34px;
      box-sizing: border-box;
    }
  }
  .tags-component {
    display: flex;
    flex-wrap: wrap;
    justify-content: flex-start;
    overflow: hidden;
  }
`

type Props = {
  onChangeTags: (tags: Array<string>) => void,
  tags: Array<string>,
  renderTag: () => Node,
  renderLayout: () => Node,
  onRemoveTag: (key: string) => void,
}

@observer
export default class TagsInputForm extends Component<Props> {
  @observable tags = []
  @observable tagsChanged = false

  @action.bound
  onChangeTags(tags: Array<string>) {
    this.tagsChanged = true
    const { onChangeTags } = this.props
    if (!onChangeTags) {
      this.tags = tags
      return
    }

    onChangeTags(tags)
  }

  @action.bound
  onRemoveTag(key: string, onRemove: () => void) {
    const { onRemoveTag = onRemove } = this.props
    onRemoveTag(key)
  }

  @autobind
  renderTag({ tag, onRemove, key }: { tag: string, onRemove: (key: number) => void, key: number }) {
    return (
      <span className="tag-item" key={key}>
        {tag} <Icon iconName="times" onClick={() => this.onRemoveTag(key, onRemove)} />
      </span>
    )
  }

  @autobind
  renderDefaultLayout(tagComponents: React$Node<*>, inputComponent: React$Node<*>) {
    return (
      <div className="tags-layout">
        <div className="input-component">{inputComponent}</div>
        <div className="tags-component">{tagComponents}</div>
      </div>
    )
  }

  render() {
    return (
      <StyledTagsInput>
        <TagsInput
          renderLayout={this.props.renderLayout || this.renderDefaultLayout}
          value={this.props.tags || this.tags}
          onChange={this.onChangeTags}
          renderTag={this.props.renderTag || this.renderTag}
          onlyUnique
        />
      </StyledTagsInput>
    )
  }
}
