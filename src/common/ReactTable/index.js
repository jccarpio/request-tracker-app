/* @flow */
import 'react-table/react-table.css'
import { action, observable, toJS } from 'mobx'
import { apiHasErrors } from '../../Helpers/API'
import { fetchTableList } from './service'
import { injectGlobal } from 'styled-components'
import { observer } from 'mobx-react'
import { Pagination } from 'react-bootstrap'
import _ from 'lodash'
import React, { Component } from 'react'
import RT from 'react-table'

injectGlobal`
.ReactTableContainer {
  .ReactTable {
    border: none !important;
    .rt {
      &-table {

      }
      &-thead{}
      &-th {
        outline:none !important;
        text-align:left !important;
        border-right:none !important;
        border-bottom: 1px solid #797979;
        font-weight:600;
        display: inline-flex;
        &:after {
          margin-left:5px;
          margin-top: 1px;
          display: inline-block;
          font: normal normal normal 14px/1 FontAwesome;
          font-size: inherit;
          text-rendering: auto;
          -webkit-font-smoothing: antialiased;
          -moz-osx-font-smoothing: grayscale;
          content: "\f0dc";
        }
        &.-sort-asc {
          box-shadow: none !important;
          &:after {
            content: "\f0de";
          }
        }
        &.-sort-desc {
          box-shadow: none !important;
          &:after {
           content: "\f0dd";
          }
        }
      }
      &-tr-group {
        border-bottom:none !important;
        &:first-child {
          margin-top:9px;
        }
      }
      &-td {
        border-right:none !important;
      }
    }
  }
  .pagination {
    li {
      &.disabled {
        span {
          border:none !important;
        }
      }
      &.active {
        span {
          background-color: #F69F32 !important;
          color :#FFF;
        }
      }
      a {
        border:none !important;
        color:#333;
      }
    }
  }
}
`
type Props = {
  data?: Array<Object>, // added ? to data to make data an optional prop
  columnDefs: Array<columnDef>,
  defaultPageSize: number,
  showPagination: boolean,
  endpoint: string,
  isServerSide: boolean,
}

type columnDef = {
  Header: string,
  accessor: string,
  Cell?: (d: any) => React$Node<any>,
}

@observer
export default class ReactTable extends Component {
  props: Props

  // observable only when the data is dynamic
  @observable isLoading: boolean = false
  @observable data: Array<Object> = this.props.data || []
  @observable currPage = 1
  @observable totalPages = 1
  @observable
  sort = {
    id: '',
    desc: false,
  }

  @action.bound
  async fetchTableList(params?: any) {
    this.isLoading = true

    const { data, errors } = await fetchTableList(this.props.endpoint, params)
    this.isLoading = false
    if (apiHasErrors({ data, errors })) {
      this.data = []
      return
    }
    const { content, totalPages } = data
    this.data = content
    this.totalPages = totalPages
  }

  renderPageNumbers(): React$Node<any> {
    const { totalPages } = this
    return [...Array(totalPages).keys()].map(pageNum => {
      const page = pageNum + 1
      return (
        <Pagination.Item
          onClick={() => this.onChangePage(page)}
          disabled={this.currPage === page || this.isLoading}
          active={this.currPage === page}
          key={page}
        >
          {page}
        </Pagination.Item>
      )
    })
  }

  renderPagination(): React$Node<any> {
    return (
      <div className="pull-right">
        <Pagination>
          <Pagination.Prev onClick={this.onPreviousPageClick} disabled={this.currPage === 1 || this.isLoading}>
            Previous
          </Pagination.Prev>
          {this.renderPageNumbers()}
          <Pagination.Next
            onClick={this.onNextPageClick}
            disabled={this.totalPages === this.currPage || this.isLoading}
          >
            Next
          </Pagination.Next>
        </Pagination>
      </div>
    )
  }

  @action.bound
  onPreviousPageClick() {
    if (this.currPage === 1) {
      return
    }
    this.currPage = this.currPage - 1
    if (this.props.isServerSide) {
      this.fetchTableList({
        number: this.currPage,
        size: this.props.defaultPageSize,
        sort: this.sort.id ? { ...this.sort } : null,
      })
    }
  }

  @action.bound
  onNextPageClick() {
    if (this.totalPages === this.currPage) {
      return
    }
    this.currPage = this.currPage + 1
    if (this.props.isServerSide) {
      this.fetchTableList({
        number: this.currPage,
        sort: this.sort.id ? { ...this.sort } : null,
        size: this.props.defaultPageSize,
      })
    }
  }

  @action.bound
  onChangePage(pageNum: number) {
    if (this.currPage === pageNum || this.isLoading) {
      return
    }
    this.currPage = pageNum
    if (this.props.isServerSide) {
      this.fetchTableList({
        number: this.currPage,
        sort: this.sort.id ? { ...this.sort } : null,
        size: this.props.defaultPageSize,
      })
    }
  }

  @action.bound
  onSortChange(params: Array<Object>) {
    this.sort = {
      ...params[0],
    }
    const { id, desc } = params[0]
    if (this.props.isServerSide) {
      this.fetchTableList({
        number: this.currPage,
        size: this.props.defaultPageSize,
        sort: {
          id: id,
          desc: desc,
        },
      })
    }
  }

  componentDidMount() {
    if (this.props.isServerSide) {
      this.fetchTableList({
        number: this.currPage,
        size: this.props.defaultPageSize,
      })
    }
  }

  render() {
    let { columnDefs, defaultPageSize, showPagination } = this.props
    return (
      <div className="ReactTableContainer">
        <RT
          data={toJS(this.data)} // converted mobx array to plain js object since React-table gives a warning
          columns={columnDefs}
          defaultPageSize={defaultPageSize}
          showPagination={showPagination}
          loading={this.isLoading}
          onSortedChange={this.onSortChange}
        />
        {this.renderPagination()}
      </div>
    )
  }
}
