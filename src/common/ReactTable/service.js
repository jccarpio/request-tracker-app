import { get } from '../../Helpers/API'

export const fetchTableList = (url, params) => {
  const { size, number, sort = null } = params
  return get(`/api/${url}?page=${number}&size=${size}${sort ? `&sort=${sort.id},${sort.desc ? 'desc' : 'asc'}` : ''}`)
}
