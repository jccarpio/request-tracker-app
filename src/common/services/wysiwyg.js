import { post } from '../../Helpers/API'

export const scanAttachments = (attachments: *) => {
  return post('/api/scans/attachment', attachments, {
    headers: {
      'Content-Type': 'multipart/form-data',
    },
  })
}

export const uploadAttachments = (attachments: *, attachmentUniqueId: *) => {
  return post(`/api/attachments${attachmentUniqueId ? `?uniqueId=${attachmentUniqueId}` : ''}`, attachments, {
    headers: {
      'Content-Type': 'multipart/form-data',
    },
  })
}
