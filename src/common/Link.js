/* @flow */

import { Button } from '../Styled'
import { NavLink } from 'react-router-dom'
import React, { PureComponent } from 'react'

export default class Link extends PureComponent {
  render() {
    const { children, to } = this.props
    return (
      <NavLink to={to} activeClassName="active">
        <Button {...this.props}>{children}</Button>
      </NavLink>
    )
  }
}
