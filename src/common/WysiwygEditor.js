/* @flow */

import 'node_modules/react-draft-wysiwyg/dist/react-draft-wysiwyg.css'
import { action, observable } from 'mobx'
import { ContentState, convertFromHTML, convertToRaw, EditorState } from 'draft-js'
import { Editor } from 'react-draft-wysiwyg'
import { FormGroup, HelpBlock } from 'react-bootstrap'
import { Icon } from '../Styled'
import { injectGlobal } from 'styled-components'
import { observer } from 'mobx-react'
import { uploadAttachments } from './services/wysiwyg'
import _ from 'lodash'
import draftToHtml from 'draftjs-to-html'
import getIconClassnameFromFile from '../Helpers/getIconClassnameFromFile'
import htmlToDraft from 'html-to-draftjs'
import React, { Component } from 'react'

injectGlobal`
  .wysiwyg-toolbar {
    background:#F5F5F5;
    button {
      border: none;
      background: transparent;
      padding: 0;
      &:disabled{
        opacity: 0.5
      }
    }
    .rdw-option-wrapper {
      border:none;
      background:inherit;
      .rdw-dropdown-wrapper {
        background:#F5F5F5;
      }
    }
  }
  .wysiwyg-wrapper {
    padding:5px 20px;
  }
  .wysiwyg-editor {
    border:1px solid #cccc;
    border-radius:5px;
    padding:2px 10px;
    height:150px;
  }
  .wysiwyg{
    &-attachments {
      min-height:50px;
      max-height:100px;
      border:1px solid #ccc;
      padding:2px;
      overflow:hidden;
      overflow-y:scroll;
      position:relative;
      &-container {
        margin:0 20px;
      }
    }
    &-file {
      &-item {
        background-color: #f5f5f5;
        max-width:180px;
        border:1px solid #e7e7e7;
        display:inline-flex;
        border-radius: 3px;
        margin: 8px 8px 0;
        position:relative;
        &:hover{
          .wysiwyg-file-action{
            display:block;
          }
        }
        &.infected, &.clean {
          .wysiwyg-file-type, .wysiwyg-file-name, .wysiwyg-file-status {
            color:#FFF !important;
          }
          .wysiwyg-file-status {
            font-size:10px;
          }
        }
        &.infected {
          background-color:#eb4d4b;
        }
        &.clean {
          background-color:#6ab04c;
        }
      }
      &-type{
        width:45px;
        width:45px;
        border:1px solid #e7e7e7;
        display:inline-block;
        padding: 10px 13px;
        .fa {
          font-size:20px;
        }
      }
      &-details{
        width:130px;
        display:inline-block;
        padding:2px;
      }
      &-name{
        white-space: nowrap;
        overflow: hidden;
        text-overflow:ellipsis;
      }
      &-status{
        font-size:10pt;
        color:  #999;
      }
      &-action {
        display:none;
        width: 30px;
        position: absolute;
        right: 0;
        background-color: rgba(255,255,255,0.5);
        height: 100%;
        padding: 12px 9px;
        cursor:pointer;
        margin: 0 !important;
        .fa {
          color:#444;;
          font-size:18px;
        }
      }
    }
  }
  .rdw-link-modal {
    height:auto !important;
  }

  .has-error {
    .wysiwyg-editor {
      border: 1px solid #a94442;
    }
    .help-block {
      margin: 5px 0px 10px;
    }
  }

  blockquote{
    background-color: rgb(240,240,240);
    margin-top: 20px;
    padding: 10px !important;
    box-shadow: -2px 2px 3px rgba(0,0,0,0.2)
  }
`

type Props = {
  onChange: (value: string, hasText: boolean) => void,
  onBlur: (value: string, hasText: boolean) => void,
  textFieldHelpBlock?: ?string,
  attachmentsHelpBlock?: ?string,
  onAttachmentUploadStart?: () => void,
  onAttachmentUploadComplete: (attachmentUniqueId: string, hasVirus: boolean) => void,
  onAttachmentUploadFailed: () => void,
}

type Attachment = {
  name: string,
  type: string,
  size: number,
  fileObject: File,
  isClean: boolean,
}

@observer
export default class WysiwygEditor extends Component {
  props: Props
  @observable editorState: * = EditorState.createEmpty()
  @observable isUploadingAttachments: boolean = false
  @observable attachments: Array<Attachment> = []
  @observable uploadAttachmentFailed: boolean = false
  @observable attachmentUniqueId: string = ''
  @observable attachmentsWithVirus: Array<Attachment> = []

  @action.bound
  clearEditorState() {
    this.editorState = EditorState.createEmpty()
    this.attachments = []
  }

  @action.bound
  handleEditorStateChange(editorState: any) {
    const { onChange } = this.props
    this.editorState = editorState
    const value = draftToHtml(convertToRaw(editorState.getCurrentContent()))
    const hasText = editorState.getCurrentContent().hasText()
    onChange(value, hasText)
  }

  @action.bound
  onSetEditorState(html: any) {
    // const stripComments = html.replace(/<!--[\s\S]*?-->/g, '')

    // const iframedHtml = `<div dangerouslySetInnerHTML={{ __html: ${html} }} />`
    const contentBlock = html.includes('<html>') ? htmlToDraft(html) : convertFromHTML(html)
    // const contentBlock = htmlToDraft(html)
    const contentState = ContentState.createFromBlockArray(contentBlock)
    const editorState = EditorState.createWithContent(contentState)
    this.editorState = editorState
  }

  @action.bound
  handleEditorBlur() {
    const { onBlur } = this.props
    const value = draftToHtml(convertToRaw(this.editorState.getCurrentContent()))
    const hasText = this.editorState.getCurrentContent().hasText()
    onBlur(value, hasText)
  }

  @action.bound
  handleAttachmentChange(e: SyntheticInputEvent<>) {
    let attachments = e.target.files
    this.onAttachmentsUpload(attachments)
  }

  @action
  async onAttachmentsUpload(files: FileList) {
    this.uploadAttachmentFailed = false
    this.isUploadingAttachments = true
    if (this.props.onAttachmentUploadStart) {
      this.props.onAttachmentUploadStart()
    }
    let formAttachment = new FormData()
    _.each(files, file => {
      formAttachment.append('files', file)
    })
    const { data, errors } = await uploadAttachments(formAttachment, this.attachmentUniqueId)
    if (!_.isEmpty(data)) {
      this.onAttachmentsUploadSuccess(data, files)
    } else {
      this.onAttachmentsUploadFailed(errors)
    }
  }

  @action
  onAttachmentsUploadSuccess(data: any, files: FileList) {
    const { infectedFiles, attachmentUniqueId } = data
    this.isUploadingAttachments = false
    let attachments = [...this.attachments]
    _.each(files, file => {
      attachments.push({
        name: file.name,
        type: file.type,
        size: file.size,
        fileObject: file,
        isClean: !_.find(infectedFiles, { fileName: file.name }),
      })
    })
    this.attachments = [...attachments]
    this.attachmentsWithVirus = _.filter(this.attachments, { isClean: false })

    if (!this.attachmentUniqueId) {
      // set attachmentUniqueId once per session
      this.attachmentUniqueId = attachmentUniqueId
    }
    this.props.onAttachmentUploadComplete(this.attachmentUniqueId, this.attachmentsWithVirus.length)
  }

  @action
  onAttachmentsUploadFailed(errors: Array<any>) {
    this.uploadAttachmentFailed = true
    this.isUploadingAttachments = false
    this.props.onAttachmentUploadFailed()
  }

  @action.bound
  deleteFileAttachment(index: number) {
    let attachments = [...this.attachments]
    attachments.splice(index, 1)
    this.attachments = attachments
    const attachmentsWithVirus = _.filter(this.attachments, { isClean: false })
    this.attachmentsWithVirus = attachmentsWithVirus
    this.props.onAttachmentUploadComplete(this.attachmentUniqueId, this.attachmentsWithVirus.length)
  }

  renderAttachmentToolbar(): React$Node<*> {
    return (
      <button
        className="rdw-link-wrapper"
        aria-label="rdw-link-control"
        disabled={this.isUploadingAttachments}
        onClick={e => {
          e.preventDefault()
          const attachmentsInputEl = document.getElementById('attachments')
          if (attachmentsInputEl) {
            attachmentsInputEl.click()
          }
        }}
        title="Attach Files"
      >
        <div className="rdw-option-wrapper" title="Link">
          <Icon iconName="paperclip" />
        </div>
      </button>
    )
  }

  renderAttachments() {
    return (
      <div className="wysiwyg-attachments-container">
        <FormGroup className={`${this.attachmentsWithVirus.length ? 'has-error' : ''} `}>
          <h6>Attachments:</h6>
          {this.isUploadingAttachments && (
            <p>
              Uploading <Icon iconName="spinner fa-spin" />
            </p>
          )}
          {this.uploadAttachmentFailed && (
            <p>
              Failed to upload attachments, please try again. <Icon iconName="fa-exlamation-triangle" />
            </p>
          )}
          {!!this.attachments.length && (
            <div className="wysiwyg-attachments">
              {this.attachments.map((file: Attachment, index: number) => {
                return (
                  <div
                    className={`wysiwyg-file-item ${file.isClean ? 'clean' : 'infected'} `}
                    key={index}
                    title={file.name}
                  >
                    <div className="wysiwyg-file-type">
                      <Icon iconName={getIconClassnameFromFile(file.fileObject)} />
                    </div>
                    <div className="wysiwyg-file-details">
                      <div className="wysiwyg-file-name">{file.name}</div>
                      <div className="wysiwyg-file-status">
                        {file.isClean ? `${file.size} KB` : `virus found please remove`}{' '}
                      </div>
                    </div>
                    <button
                      className="wysiwyg-file-action"
                      onClick={e => {
                        e.preventDefault()
                        this.deleteFileAttachment(index)
                      }}
                    >
                      <Icon iconName="close" />
                    </button>
                  </div>
                )
              })}
            </div>
          )}
          {this.attachmentsWithVirus.length ? (
            <HelpBlock>Virus found, one or more of your attachment(s) was infected, please remove it.</HelpBlock>
          ) : null}
        </FormGroup>
      </div>
    )
  }

  render() {
    const { textFieldHelpBlock } = this.props
    const shouldRenderAttachmentsBlock =
      this.attachments.length || this.isUploadingAttachments || this.uploadAttachmentFailed
    return (
      <div>
        <Editor
          toolbar={{
            options: ['fontFamily', 'fontSize', 'blockType', 'textAlign', 'inline', 'list', 'link', 'history'],
            inline: { inDropdown: true },
            list: { inDropdown: true },
            textAlign: { inDropdown: true },
            link: { inDropdown: true },
            history: { inDropdown: false },
          }}
          toolbarCustomButtons={[this.renderAttachmentToolbar()]}
          editorState={this.editorState}
          onEditorStateChange={this.handleEditorStateChange}
          onBlur={this.handleEditorBlur}
          toolbarClassName="wysiwyg-toolbar"
          wrapperClassName="wysiwyg-wrapper"
          editorClassName="wysiwyg-editor"
        />
        {!!textFieldHelpBlock && <HelpBlock>{textFieldHelpBlock}</HelpBlock>}
        {shouldRenderAttachmentsBlock && this.renderAttachments()}
        <input type="file" multiple className="hidden" id="attachments" onChange={this.handleAttachmentChange} />
      </div>
    )
  }
}
