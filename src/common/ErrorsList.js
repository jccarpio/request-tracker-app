/* @flow */

import { type ApiError } from '../Helpers/API'
import { COLOR_ERROR } from '../Styled/themeColours'
import { Panel } from '../Styled'
import React, { PureComponent } from 'react'
import styled from 'styled-components'

const StyledErrorList = styled(Panel)`
  .errors {
    text-align: left;
    padding: 0;
    margin: 0 0 0 15px;
    color: ${COLOR_ERROR};
  }
`

type Props = {
  errors: Array<ApiError>,
}

export default class ErrorsList extends PureComponent {
  props: Props
  render() {
    const { errors } = this.props
    return (
      <StyledErrorList panelColor="error" className="errors-list">
        <ul className="errors">
          {errors.map((error, index) => {
            return (
              <li key={index}>
                {error.fieldName ? `${error.fieldName}:` : ''} {error.message}
              </li>
            )
          })}
        </ul>
      </StyledErrorList>
    )
  }
}
