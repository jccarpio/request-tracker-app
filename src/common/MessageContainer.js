/* @flow */

import { Col, Row } from 'react-bootstrap'
import { type TicketAttachment } from '../ticket/types'
import Attachment from '../ticket/Attachment'
import Icon from '../common/Icon'
import React, { PureComponent } from 'react'
import styled from 'styled-components'

const OWNER_COLOR_ROLE = Object.freeze({
  ROLE_USER: '#F7F7F7',
  ROLE_AGENT: '#E6E6FF',
  ROLE_ADMIN: '#E6E6FF',
  ROLE_VIEWER: '#CEFFDE',
})

const StyledContainer = styled.div`
  blockquote {
    border: 1px solid rgb(220, 220, 220);
    padding: 20px;
    margin: 20px;
    background-color: rgba(0, 0, 0, 0.1);
  }
  position: relative;
  .bubble-container {
    background-color: ${props => OWNER_COLOR_ROLE[props.ownerRole] || OWNER_COLOR_ROLE.ROLE_ADMIN};
    padding: 10px;
    position: relative;
    margin-left: ${props => (props.isRight ? '0' : '15px')};
    margin-right: ${props => (props.isRight ? '15px' : '0px')};
    border-radius: 5px;
    max-width: 750px;
    .action-buttons {
      display: none;
    }
    &:hover {
      .action-buttons {
        display: block;
      }
    }
  }
  .bubble-container {
    box-shadow: ${props => (props.isRight ? '-2px 2px 3px rgba(0, 0, 0, 0.2)' : '2px 2px 3px rgba(0, 0, 0, 0.2)')};
  }
  .arrow {
    background-color: ${props => OWNER_COLOR_ROLE[props.ownerRole] || OWNER_COLOR_ROLE.ROLE_ADMIN};
    position: absolute;
    width: 30px;
    height: 30px;
    transform: rotate(45deg);
    top: 15px;
    box-shadow: 2px 2px 3px rgba(0, 0, 0, 0.2);
  }
  .arrow-left {
    left: 0;
  }
  .arrow-right {
    right: 0;
  }
  .headerContainer {
    font-size: 24px;
    div {
      padding: 0;
    }
    .action-buttons {
      i {
        display: inline-block;
        margin: 0 3px;
      }
    }
  }
  .row {
    margin: 0;
  }
  .author {
    color: #337ab7;
    font-size: 18px;
  }
  .timestamp {
    font-size: 12px;
    margin-left: 5px;
  }
  .creator {
    color: #337ab7;
    font-size: 12px;
  }
  .creator-container {
    color: rgb(153, 153, 153);
    font-size: 12px;
  }
  .author-container {
    .timestamp {
      color: rgba(119, 119, 119, 1);
    }
  }
  .message-container {
    margin-top: 5px;
  }
  .scroll-horizontal {
    overflow-x: auto;
  }
  .loading-container {
    padding: 20px;
    text-align: center;
  }
  .attachments {
    padding: 0 15px;
    display: flex;
    align-items: center;
    // overflow: hidden;
    // overflow-x: auto;
    // max-height: 200px;
    width: 100%;
  }
  .attachments-header {
    margin-bottom: 10px;
  }
`

type Props = {
  title?: string,
  author?: string,
  ownerRole: 'client' | 'peplink' | 'owner',
  repliedTimeStamp: string,
  creator?: string,
  createdTimeStamp?: string,
  isRight?: boolean,
  children: any,
  onDeletePressed?: () => void,
  isLoading?: boolean,
  attachments?: ?Array<TicketAttachment>,
}

export default class MessageContainer extends PureComponent {
  props: Props
  renderArrow() {
    return <div className={`${this.props.isRight ? 'arrow-right' : 'arrow-left'} arrow`} />
  }

  renderHeader() {
    return (
      <Row className="headerContainer">
        <Row>
          <Col md={8}>
            <div>{this.props.title}</div>
            {this.renderAuthor()}
            {Boolean(this.props.creator) && this.renderCreator()}
          </Col>
          <Col md={2} className="pull-right text-right action-buttons">
            {/* {this.props.onReplyPressed && <Icon iconName="reply" onClick={this.props.onReplyPressed} xlarge />} */}
            {this.props.onReplyPressed && <Icon iconName="reply" onClick={() => alert('reply was pressed')} xlarge />}
            {this.props.onDeletePressed && <Icon iconName="trash" onClick={this.props.onDeletePressed} xlarge />}
          </Col>
        </Row>
      </Row>
    )
  }

  renderCreator() {
    const { creator, createdTimeStamp } = this.props
    return (
      <Row className="creator-container">
        <span>
          Created By <a className="creator">{creator}</a>
        </span>
        <span className="timestamp">{createdTimeStamp}</span>
      </Row>
    )
  }

  renderAuthor() {
    const { author, repliedTimeStamp } = this.props
    return (
      <Row className="author-container">
        <a className="author">{author}</a>
        <span className="timestamp">{repliedTimeStamp}</span>
      </Row>
    )
  }

  renderLoading() {
    return (
      <div className="loading-container">
        <Icon iconName="spinner" large />
      </div>
    )
  }

  renderMessageContent() {
    const { children: content, attachments } = this.props
    return (
      <div>
        {this.renderHeader()}
        <Row className="message-container">
          <div>
            <div className="scroll-horizontal">{content}</div>
            {attachments && attachments.length ? (
              <div className="scroll-horizontal">
                <span className="attachments-header">Attachments:</span>
                {this.renderAttachments()}
              </div>
            ) : null}
          </div>
        </Row>
      </div>
    )
  }

  renderAttachments() {
    const { attachments } = this.props
    if (!attachments || !attachments.length) {
      return null
    }
    return (
      <div>
        {_.map(_.chunk(attachments, 3), (attachmentsRow, rowIndex) => {
          return (
            <div className="attachments">
              {_.map(attachmentsRow, (attachment, index) => (
                <Attachment key={index} data={attachment} hideSubInfo showThumbnail />
              ))}
            </div>
          )
        })}
      </div>
    )
  }

  render() {
    const { isRight = false, ownerRole, isLoading } = this.props

    return (
      <StyledContainer isRight={isRight} ownerRole={ownerRole}>
        {this.renderArrow()}
        <div className={`${ownerRole} bubble-container`}>
          {isLoading ? this.renderLoading() : this.renderMessageContent()}
        </div>
      </StyledContainer>
    )
  }
}
