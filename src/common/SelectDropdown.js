/* @flow */

import { FormControl } from 'react-bootstrap'
import { injectGlobal } from 'styled-components'
import React, { PureComponent } from 'react'

injectGlobal`
  .Select-menu-outer{
    z-index: 999;
  }
`

type Props = {
  onChange: (event: any) => void,
  options: Array<{ value: string, text: string }> | Array<string>,
  name: string,
  placeholder?: string,
  value: any,
  disabled?: boolean,
  withOutDefault?: boolean,
}

export default class SelectDropdown extends PureComponent<Props> {
  renderOptions() {
    return this.props.options.map((option, index) => {
      let value = option
      let text = option
      if (typeof option !== 'string') {
        value = option.value
        text = option.text
      }
      return (
        <option key={index} value={value}>
          {text}
        </option>
      )
    })
  }

  render() {
    const { onChange, name, placeholder, value, disabled, withOutDefault = false } = this.props
    return (
      <FormControl
        disabled={disabled}
        name={name}
        componentClass="select"
        placeholder={placeholder}
        onChange={onChange}
        value={value}
      >
        {withOutDefault ? null : <option value="">{placeholder || 'Please choose'}</option>}

        {this.renderOptions()}
      </FormControl>
    )
  }
}
