/* @flow */

import { Panel as _Panel, Col } from 'react-bootstrap'
import LineChart from './LineChart'
import React, { PureComponent } from 'react'
import styled from 'styled-components'
const Panel = styled(_Panel)`
  border-radius: 15px;
  border: 1px solid #999999;
`
const PanelHeader = styled(_Panel.Heading)`
  background-color: #fff !important;
  border-bottom: 1px solid #999999 !important;
  border-radius: 15px;
  border-bottom-left-radius: 0;
  border-bottom-right-radius: 0;
  padding: 0;
  h4 {
    font-weight: 300;
    font-size: 20px;
    padding: 10px 15px;
  }
`
const PanelBody = styled(_Panel.Body)`
  padding: 15px 30px;
`

export default class PanelChart extends PureComponent<Props> {
  render() {
    const { title, data } = this.props
    return (
      <Panel>
        <PanelHeader>
          <h4>{title}</h4>
        </PanelHeader>
        <PanelBody>
          <Col lg={2}>
            <h2>999</h2>
            <h4>Latest</h4>
          </Col>
          <Col lg={10}>
            <LineChart data={data} />
          </Col>
        </PanelBody>
      </Panel>
    )
  }
}
