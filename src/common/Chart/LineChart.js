/* @flow */

import { Line } from 'react-chartjs-2'
import React, { PureComponent } from 'react'
import styled from 'styled-components'

export default class LineChart extends PureComponent<Props> {
  render() {
    const { data } = this.props
    return <Line data={data} />
  }
}
