/* @flow */

import { action, observable } from 'mobx'
import { COLOR_TERTIARY } from '../Styled/themeColours'
import { observer } from 'mobx-react'
import { Panel } from 'react-bootstrap'
import Icon from './Icon'
import React, { Component } from 'react'
import styled from 'styled-components'

const StyledCollapsibleWidget = styled(Panel)`
  background-color: transparent;
  border: none;
  box-shadow: none;
  .panel-heading {
    display: inline-block;
    min-width: 200px;
    background-color: ${COLOR_TERTIARY};
    padding: 0;
    border-radius: 10px 10px 0 0;
    margin-left: 10px;
    a {
      color: rgba(255, 255, 255, 1);
      display: inline-block;
      padding: 10px;
    }
  }
  .panel-heading-text {
    color: rgba(255, 255, 255, 1);
    display: block;
    padding: 10px;
  }
  .panel-content {
    background-color: rgba(255, 255, 255, 1);
    border-radius: 10px;
    border: 1px solid rgba(204, 204, 204, 1);
    margin-top: -2px;
  }
`

@observer
export default class CollapsibleWidget extends Component {
  @observable isOpen = true

  @action.bound
  toggleCollapse(test: string) {
    this.isOpen = !this.isOpen
  }
  render() {
    const { children, toggleButton, hasEdit = false, editAction } = this.props
    const iconName = this.isOpen ? 'chevron-up' : 'chevron-down'
    return (
      <StyledCollapsibleWidget defaultExpanded>
        <Panel.Heading className="panel-heading">
          <Panel.Toggle className="panel-heading-text" onClick={this.toggleCollapse}>
            <Icon iconName={iconName} /> {toggleButton}
          </Panel.Toggle>
          {hasEdit ? (
            <a href="javascript:void(0)" onClick={editAction} className="pull-right">
              edit
            </a>
          ) : null}
        </Panel.Heading>
        <Panel.Collapse className="panel-content">{children}</Panel.Collapse>
      </StyledCollapsibleWidget>
    )
  }
}
