/* @flow */

import TicketRepository from './ticket/TicketRepository'
import UserRepository from './main/UserRepository'

const userRepository = new UserRepository()
export default {
  userRepository: userRepository,
  ticketRepository: new TicketRepository(userRepository),
}
